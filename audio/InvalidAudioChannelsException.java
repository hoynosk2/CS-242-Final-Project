package audio;

/**
 * @description Indicates when a number of channels is unsupported.
 * @author dan
 *
 */
public class InvalidAudioChannelsException extends Exception{
	/**
	 * Java Documentation:
	 * The serialization runtime associates with each serializable class a version number, 
	 * called a serialVersionUID, which is used during deserialization to verify that the sender 
	 * and receiver of a serialized object have loaded classes for that object that are compatible 
	 * with respect to serialization.
	 */
	private static final long serialVersionUID = 1L;
	private static final String MSG = "Only a single channel of audio (i.e. mono) is supported. The following number of channels is unsupported: ";
	public InvalidAudioChannelsException(int numberOfChannels) {
		super(MSG + numberOfChannels);
	}
}
