package audio;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class AudioCapture {
	public static final AudioFormat STD_FORMAT = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED /* Encoding */, 
																44100.0f /* Sample Rate */, 
																16 /* Sample Rate Size in Bits */, 
																1 /* Number of Channels (1=Mono, 2=Stereo) */, 
																2 /* Frame Size */, 
																44100.0f /* Frame Rate */, 
																false /* Is Big Endian? */);
	public static final AudioFileFormat.Type STD_TYPE = AudioFileFormat.Type.WAVE; 
	
	public static final String AUDIO_BASE_DIR_MAC = "/Volumes/Audio/Korean Text-to-Speech Audio";
	public static final String TEST_DIR = "/Users/Dan/Desktop/Audio";
	public static final String AUDIO_BASE_DIR_WINDOWS = ""; // TODO get a windows directory address
	
	private final long recordTime; 	// The duration audio will be recorded for.
	private String recordFileName;
	private TargetDataLine line;		// The line that is used to capture the audio
	private File recordFile;
	private Timer timer;
	private boolean valid = true, isRecording = false;
	
	public AudioCapture(final long recordTime) {
		this.recordTime = recordTime;
		timer = new Timer();
	}
	
	public void setRecordFileName(String recordFileName){
		this.recordFile = new File(TEST_DIR + "/" + recordFileName + ".wav");
	}
	
	public void setRecordFile(File recordFile) {
		this.recordFile = recordFile;
	}
	
	public File getRecordFile() {
		return this.recordFile;
	}
	
	/**
	 * @description Details if the AudioCapture object is still valid.
	 * @return
	 */
	public boolean isValid() {
		return valid;
	}
	
	public boolean isRecording() {
		if(line == null)
			return false;
		return line.isOpen() || line.isActive() || line.isRunning() || isRecording;
	}

	public void start() throws LineUnavailableException, IOException {
		if(recordFile == null) {
			throw new IOException();
		}
				
		DataLine.Info info = new DataLine.Info(TargetDataLine.class, STD_FORMAT);
		
		// Indicates whether the system supports any lines that match the specified Line.Info object. 
		// A line is supported if any installed mixer supports it.
		if(!AudioSystem.isLineSupported(info)) {
			throw new LineUnavailableException();
		}
		
		line = (TargetDataLine) AudioSystem.getLine(info);
		line.open(STD_FORMAT);
		line.start();   // Start capturing of audio (Allows a line to engage in data I/O).
		AudioInputStream ais = new AudioInputStream(line);
		
		// AudioSystem.write() is a blocking call, therefore package in thread.
		(new Thread() {
			@Override
			public void run() {
				try {
					/*
					 * Note: VERY VERY important to have this boolean here. 
					 * If AudioSystem does not finish writing in time (i.e. main thread ends earlier),  
					 * programs like QuickTime will throw an error when trying to play the WAV file.
					 */
					isRecording = true;
					AudioSystem.write(ais, STD_TYPE, recordFile); // Starts recording of audio.
					isRecording = false;
				} catch (IOException e) {
					valid = false;
				} 
			}
		}).start();
		
		// Start timer after beginning recording to avoid any clipping of audio (little extra doesn't hurt).
		timer.schedule(new TimerTask() {
			@Override
			public void run() {
				stop();
			}
		}, recordTime * 1000); 
	}
	
	public void stop() {
		line.stop();
        line.close();
	}
}
