package audio;

import java.io.IOException;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;

/**
 * @description Plays audio data to speakers.
 * @author dan
 *
 */
public class AudioPlayer {
	private Clip clip;
	
	/**
	 * @description Creates a player for playing audio data.
	 * @throws LineUnavailableException If unable to play through speaker.
	 */
	public AudioPlayer() throws LineUnavailableException {
		_initialize();
	}
	
	/**
	 * @description Get the audio clip for playing through the player.
	 * @throws LineUnavailableException
	 */
	public void _initialize() throws LineUnavailableException{
		this.clip = AudioSystem.getClip();
	}
	
	/**
	 * @description Plays audio through speaker.
	 * @param audio The audio to play.
	 * @throws IOException
	 * @throws LineUnavailableException
	 */
	public void play(AudioData audio) throws IOException, LineUnavailableException {
		if(!clip.isOpen())
			clip.open(audio.getInputStream());
		clip.start();
	}
	
	/**
	 * @description Resets the clip to play another audio file.
	 */
	public void reset() {
		clip.flush();
		if(clip.isActive())
			clip.stop();
		if(clip.isOpen())
			clip.close();
	}
	
	/**
	 * @description Stops the audio from playing immediately.
	 */
	public void stop() {
		reset();
	}
}
