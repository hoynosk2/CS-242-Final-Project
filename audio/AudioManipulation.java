package audio;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.io.SequenceInputStream;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.UnsupportedAudioFileException;

/**
 * @description The audio manipulation API
 * @author dan
 * @functions Combine audio files together, add in fade in and out of audio, change volume of audio, and remove silence from audio.
 */
public class AudioManipulation {
	private static final int SIZE_OF_BYTE = 8;
	private static final int BYTE_MASK = 0xff;
	
	public static final int FADE_IN = 0; 
	public static final int FADE_OUT = 1;
	
	public static final int SILENCE_SECTION_DURATION = 2; 	// In seconds
	public static final int SPEAKING_SECTION_DURATION = 3;	// In seconds
	public static final int DEFAULT_SILENCE_BUFFER = 0;		// A tweaking variable used to either raise or lower the silence finder's tolerance.
	
	/**
	 * @description Combines to audio files together as one.
	 * @param audio1 The starting audio file.
	 * @param audio2 The ending audio file.
	 * @return The combined audio data as one singleton.
	 * @throws IOException
	 * @throws NullPointerException - No audio data has been generated in one or more parameter(s)
	 * @note Audio1's format is used throughout, therefore both audio data should be about the same in format.
	 */
	public static AudioData CombineAudio(AudioData audio1, AudioData audio2) throws IOException, NullPointerException {
		AudioInputStream audio1IS = audio1.getInputStream();
		AudioInputStream audio2IS = audio2.getInputStream();
		
		// Note: Audio1's format is used throughout, therefore both audio data should be about the same in format.
		AudioInputStream combinedAudio = new AudioInputStream(new SequenceInputStream(audio1IS, audio2IS),
															 audio1IS.getFormat(), 
															 audio1IS.getFrameLength() + audio2IS.getFrameLength());
		
		return new AudioData(AudioData.GetAudioData(combinedAudio), combinedAudio.getFormat());
	}
	 
	/**
	 * @description Modifies a single audio file with the correct single audio manipulation tools.
	 * @param audio The audio to alter
	 * @throws IOException
	 * @throws UnsupportedAudioFileException
	 */
	public static void SingleModification(AudioData audio, int silence_buffer) throws IOException, UnsupportedAudioFileException {
		AudioManipulation.RemoveSilence(audio, silence_buffer);
		AudioManipulation.FadeAudio(audio, (int)(Math.ceil(audio.getDuration()) / 2), AudioManipulation.FADE_IN);
		AudioManipulation.FadeAudio(audio, (int)(Math.ceil(audio.getDuration()) / 2), AudioManipulation.FADE_OUT);
	}
	
	/**
	 * @description Gets the threshold of silence for either the starting silence opening or the ending silence opening.
	 * @param data The audio data to analyze.
	 * @param front When true, indicates the starting silence opening, else the ending silence opening.
	 * @return The silence threshold for the given "front" section (i.e. starting or ending).
	 * @throws IOException
	 * @throws UnsupportedAudioFileException
	 */
	private static float _getSilenceThreshold(AudioData data, boolean front) throws IOException, UnsupportedAudioFileException {
		int numOfBytesInSample = (data.getSampleSizeInBits() / SIZE_OF_BYTE); 		// Either 2, 3, or 4 bytes.
		int numOfBytesPerSec = ((int)data.getSampleRate()) * numOfBytesInSample;		// Represents the number of bytes that occur in a given second of audio.
		byte[] audioData = data.getAudioData();
		
		float averageOfSilence = 0;	// Represents the average amount of noise an a given silence segment.
		float maxSilence = 0;		// The loudest noise the occurs in the silence segment.
		
		if(front) { 
			/*
			 * The starting silence segment is the first few seconds of the audio. This area is dedicated to recording
			 * the silence around. 
			 */
			for(int i = 0; i < numOfBytesPerSec * SILENCE_SECTION_DURATION; i+=2) {
				// Represents a 16-bit sample
				// WAV order is in Little-Endian, hence this picking order of the two bytes.
				short highByte = audioData[i+1];
				short lowByte = audioData[i];
				short nextSample = _createSample(highByte, lowByte);
				
				averageOfSilence += Math.abs(nextSample);
				maxSilence = Math.max(Math.abs(nextSample), maxSilence);
			}
		} else {
			/*
			 * The ending silence segment is the last few seconds of the audio. This area is dedicated to recording
			 * the silence around. 
			 * Reason: The ending segment is included, because silence conditions may have changed.
			 */
			for(int i = audioData.length - 2; i > (audioData.length - (numOfBytesPerSec)); i-=2) {
				// Represents a 16-bit sample
				// WAV order is in Little-Endian, hence this picking order of the two bytes.
				short highByte = audioData[i+1];
				short lowByte = audioData[i];
				short nextSample = _createSample(highByte, lowByte);
				
				averageOfSilence += Math.abs(nextSample);
				maxSilence = Math.max(Math.abs(nextSample), maxSilence);
			}
		}
		averageOfSilence /= ((float)numOfBytesPerSec * (float)SILENCE_SECTION_DURATION) / 2.0; 	// Must divide by the number of samples added up in the for-loop above.
		return maxSilence + averageOfSilence; 	// The averageOfSilence acts as a tiny buffer above the maxSilence.
	} 
	
	/**
	 * @description Removes silence in the beginning of the audio file and at the end of the audio file.
	 * @param data The audio data to modify
	 * @throws IOException
	 * @throws UnsupportedAudioFileException
	 */
	public static void RemoveSilence(AudioData data, int silence_buffer) throws IOException, UnsupportedAudioFileException {
		int numOfBytesInSample = (data.getSampleSizeInBits() / SIZE_OF_BYTE);		// Either 2, 3, or 4 bytes.
		int numOfBytesPerSec = ((int)data.getSampleRate()) * numOfBytesInSample;	// Represents the number of bytes that occur in a given second of audio.
		
		byte[] audioData = data.getAudioData();
		
		float thresholdForStartingSilence = _getSilenceThreshold(data, true);
		float thresholdForEndingSilence = _getSilenceThreshold(data, false);
		List<Byte> newAudioData = new ArrayList<Byte>(); 	// Will hold the non-silence audio bytes.
		int start = audioData.length, end = 0; 			// Marks the starting and ending location of non-silence audio respectively.
		
		/*
		 * Marks the starting location of non-silence audio in the beginning.
		 */
		for(int i = numOfBytesPerSec * SILENCE_SECTION_DURATION; i < audioData.length; i+=2) {
			// Represents a 16-bit sample
			// WAV order is in Little-Endian, hence this picking order of the two bytes.
			short highByte = audioData[i+1];
			short lowByte = audioData[i];
			short nextSample = _createSample(highByte, lowByte);
			
			//The moment we find a sample that surpasses our threshold, stop looking.
			if(	nextSample >= (thresholdForStartingSilence + silence_buffer) || 
				nextSample <= (-1 * thresholdForStartingSilence - silence_buffer)) {
					start = i;
					break;
			}
		}
		
		/*
		 * Marks the ending location of non-silence audio in the end.
		 */
		for(int i = audioData.length - 2; i >= numOfBytesPerSec * SILENCE_SECTION_DURATION; i-=2) {
			// Represents a 16-bit sample
			// WAV order is in Little-Endian, hence this picking order of the two bytes.
			short highByte = audioData[i+1];
			short lowByte = audioData[i];
			short nextSample = _createSample(highByte, lowByte);
			
			//The moment we find a sample that surpasses our threshold, stop looking.
			if(	nextSample >= (thresholdForEndingSilence + silence_buffer) || 
				nextSample <= (-1 * thresholdForEndingSilence - silence_buffer)) {
					end = i;
					break;
			}
		}
		
		// Copy the non-silence audio back into the audio data source except without the silence.
		for(int i = start; i < end; i++) {
			newAudioData.add(new Byte((byte) audioData[i]));
		}
		data.setAudioData(_getByteArrayFromList(newAudioData));
	}
	
	/**
	 * @description Unboxes a list of byte wrappers to a byte array.
	 * @param list
	 * @return The byte array representing the list of byte wrappers.
	 */
	private static byte[] _getByteArrayFromList(List<Byte> list) {
		byte[] data = new byte[list.size()];
		for(int i = 0; i < list.size(); i++) {
			data[i] = list.get(i);
		}
		return data;
	}
	
	/**
	 * @description Changes the volume for the entire audio source.
	 * @param data The audio data to modify.
	 * @param volume The volume level.
	 * @throws IOException
	 * @throws UnsupportedAudioFileException
	 * @note If the volume exceeds too high, some distortion will be heard.
	 */
	public static void ChangeVolume(AudioData data, double volume) throws IOException, UnsupportedAudioFileException {
		if(data.getSampleSizeInBits() == AudioData.VALID_SAMPLE_RATE_SIZE[0]) { // 16-bit Sample Size
			byte[] audioData = data.getAudioData();
			
			_changeVolume(audioData, 0, audioData.length, volume);
		}
	}
	
	/**
	 * @description Modifies the volume for a given section of audio in the raw audio data samples.
	 * @param audioData The raw audio data.
	 * @param begin The starting byte location in the raw audio data (inclusive).
	 * @param end The ending byte location in the raw audio data (exclusive).
	 * @param volume The volume to change the following section too.
	 */
	private static void _changeVolume(byte[] audioData, int begin, int end, double volume) {
		for(int i = begin; i < Math.min(audioData.length, end); i += 2) {
			// Represents a 16-bit sample
			// WAV order is in Little-Endian, hence this picking order of the two bytes.
			short highByte = audioData[i+1];
			short lowByte = audioData[i];
			short sample = _createSample(highByte, lowByte);
			
			sample = (short) (sample * volume); // Scales the amplitude of the sound sine wave.
			
			// Restore newly modified data back in Little-Endian order
			audioData[i] = (byte) sample;
			audioData[i + 1] = (byte) (sample >> SIZE_OF_BYTE);
		}
	}
	
	/**
	 * @description Creates a 16-bit audio sample from two bytes.
	 * @param highByte The high byte for the audio sample.
	 * @param lowByte The low byte for the audio sample.
	 * @return A 16-bit audio sample.
	 */
	private static short _createSample(short highByte, short lowByte) {
		highByte = (short) ((highByte & BYTE_MASK) << SIZE_OF_BYTE); // Move high byte into correct position
		lowByte = (short) (lowByte & BYTE_MASK);						// Move low byte into correct position
		
		// Combine to create a single sample from the audio.
		return (short) (highByte | lowByte);
	}
	
	/**
	 * @description Fades the audio linearly either in or out (i.e. at the beginning or ending).
	 * @param data The audio data to modify.
	 * @param duration - Number of seconds of fade time.
	 * @param type Either a fading in at the beginning or a fading out at the ending.
	 * @throws UnsupportedAudioFileException 
	 * @throws IOException 
	 */
	public static void FadeAudio(AudioData data, int duration, int type) throws IOException, UnsupportedAudioFileException {
		int numOfBytesInSample = (data.getSampleSizeInBits() / SIZE_OF_BYTE);		// Either 2, 3, or 4 bytes.
		int numOfBytesPerSec = ((int)data.getSampleRate()) * numOfBytesInSample;	// Represents the number of bytes that occur in a given second of audio.
		
		int numOfBytesInDuration = numOfBytesPerSec * duration;					// Details the number of bytes that will occur in the specified duration.
		int numOfSamplesInDuration = numOfBytesInDuration / numOfBytesInSample; 	// Details the number of samples that will occur in the specified duration.
		
		double volumeFactor = 1.0 / ((double)numOfSamplesInDuration); // The factor to increase/decrease the audio by.
		
		if(type == FADE_IN) {
			/*
			 * The FADE_IN represents the fading in of the audio in the very beginning. 
			 * It takes the length of the 'duration' to completely fade inward.
			 */
			for(int i = 0; i < numOfBytesInDuration; i+=2) {
				// 'i' is the starting position of the byte in the ith sample, 
				// therefore divide by 'numOfBytesInSample' to refocus back to sample volume rate.
				double newVolume = volumeFactor * (i / numOfBytesInSample); 
				_changeVolume(data.getAudioData(), i, i + numOfBytesInSample, newVolume);
			}
		} else {
			/*
			 * The FADE_OUT represents the fading out of the audio in the very ending. 
			 * It takes the length of the 'duration' to completely fade outward.
			 */
			int startingByte = data.getAudioData().length - numOfBytesInDuration;
			if(startingByte < 0)
				startingByte = 0;
			
			for(int i = startingByte; i < data.getAudioData().length; i+=2) {
				// 'i' is the starting position of the byte in the ith sample, 
				// therefore divide by 'numOfBytesInSample' to refocus back to sample volume rate.
				double newVolume = 1.0 - volumeFactor * ((i - startingByte) / numOfBytesInSample); 
				_changeVolume(data.getAudioData(), i, i + numOfBytesInSample, newVolume);
			}
		}
	}
}
