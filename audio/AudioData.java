package audio;
import java.io.IOException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;


/**
 * @description Represents the audio raw data and header data for a given WAV file.
 * @author dan
 * 
 */
public class AudioData {
	private static final int 	INTERMEDIATE_BUFFER_STD_LENGTH = 4096;	// Standard input stream return buffer length.
	private static final int 	MONO_SOUND = 1;							// Note: Stereo sound is currently not supported.
	private static final String 	WAV_EXTENSION = "wav";					// The file extension for a WAV file.
	
	public static final AudioData EMPTY_AUDIO = null;
	
	public static final int[] VALID_SAMPLE_RATE_SIZE = {16};				// Note: 32-bit sample rate produced errors.
	
	private final String audioFilePath; 									// The path to the raw WAV audio file.
	private byte[] audioData = null;										// The raw audio data for the WAV file.
	private AudioFormat format = null;									// Contains all the header information for the WAV file.
	
	/**
	 * @description Constructs an AudioData object. 
	 * @param audioFilePath The path to the WAV audio file.
	 * @throws UnsupportedAudioFileException
	 * @throws InvalidAudioChannelsException
	 * @throws IOException
	 */
	public AudioData(final String audioFilePath) throws UnsupportedAudioFileException, InvalidAudioChannelsException, IOException {
		this.audioFilePath = audioFilePath;
		initialization(); // A few pre-reqs must be met for everything to run smoothly.
	}
	
	/**
	 * @description Constructs an AudioData object.
	 * @param audioData The raw audio data for a given WAV file.
	 * @param format The audio header data for a given WAV file.
	 * @notes AudioFilePath is null as raw audio data and header are present.
	 */
	public AudioData(byte[] audioData, AudioFormat format) {
		this.audioData = audioData;
		this.format = format;
		this.audioFilePath = null;
	}
	
	/**
	 * @description Verifies that a WAV audio file is being used, mono sound is being used, and 16-bit sample size is being used. 
	 * @throws UnsupportedAudioFileException
	 * @throws InvalidAudioChannelsException
	 * @throws IOException
	 */
	private void initialization() throws UnsupportedAudioFileException, InvalidAudioChannelsException, IOException{
		this.format = _getAudioFormat();
		String fileType = _fileType();
		
		if(!fileType.equalsIgnoreCase(WAV_EXTENSION))
			throw new UnsupportedAudioFileException();
		
		/*
		 * Verifies that a valid sample size is being used.
		 * Note: Currently 32-bit sample size produces distorted audio and therefore is unsupported.
		 */
		boolean validSampleSize = false;
		for(int i = 0; i < VALID_SAMPLE_RATE_SIZE.length; i++) {
			if(this.format.getSampleSizeInBits() == VALID_SAMPLE_RATE_SIZE[i]) {
				validSampleSize = true;
			}
		}
		if(!validSampleSize)
			throw new UnsupportedAudioFileException();
		
		// Note: Stereo sound is currently not supported.
		if(format.getChannels() != MONO_SOUND)
			throw new InvalidAudioChannelsException(format.getChannels());
	}
	
	/**
	 * @description Gets the file type extension for the given audioFilePath.
	 * @return Extension of audioFilePath.
	 */
	private String _fileType() {
		String audioFile = new File(this.audioFilePath).getName();
		if(audioFile.lastIndexOf(".") != -1 && audioFile.lastIndexOf(".") != 0)
			return audioFile.substring(audioFile.lastIndexOf(".") + 1);
		else
			return ""; // No extension found.
	}
	
	/**
	 * @description Gets the sample rate for the given audio.
	 * @return The sample rate
	 */
	public float getSampleRate() {
		return format.getSampleRate();
	}
	
	/**
	 * @description Gets the size (in bits) for a single sample.
	 * @return Sample size in bits
	 */
	public int getSampleSizeInBits() {
		return format.getSampleSizeInBits();
	}
	
	/**
	 * @description Allows manually setting of the raw audio data.
	 * @param audioData
	 */
	public void setAudioData(byte[] audioData) {
		this.audioData = audioData;
	}
	
	/**
	 * @description Gets the current format header object for the audio file.
	 * @return The format header object (AudioFormat).
	 * @throws UnsupportedAudioFileException
	 * @throws IOException
	 */
	private AudioFormat _getAudioFormat() throws UnsupportedAudioFileException, IOException {
		File audioFile = new File(this.audioFilePath);
		AudioInputStream audioIS = AudioSystem.getAudioInputStream(audioFile);
		
		AudioFormat audioFormat = audioIS.getFormat();
		audioIS.close();

		return audioFormat;
	}
	
	/**
	 * @description Determines if the audio data has been loaded or not.
	 * @return True if audio data has been loaded, false otherwise.
	 */
	public boolean isAudioDataLoaded() {
		return (audioData != null);
	}
	
	/**
	 * @description Gets raw data that represents the current audio file.
	 * @return The raw audio data.
	 * @throws IOException
	 * @throws UnsupportedAudioFileException
	 */
	public byte[] getAudioData() throws IOException, UnsupportedAudioFileException {
		// Because costly, pull the audio data only once if not available yet.
		if(audioData == null)
			return audioData = _getAudioData();
		return audioData;
	}
	
	/**
	 * @description Reads and returns the raw audio data from the audio file.
	 * @return The raw audio data.
	 * @throws IOException
	 * @throws UnsupportedAudioFileException
	 */
	private byte[] _getAudioData() throws IOException, UnsupportedAudioFileException {
		byte[] data = null;
		File audioFile = new File(this.audioFilePath);
		AudioInputStream audioIS = AudioSystem.getAudioInputStream(audioFile); // Gets the input stream that is attached to the raw audio data chunk.
		
		data = GetAudioData(audioIS);
		
		audioIS.close();
		return data;
		
	}
	
	/**
	 * @description Reads and returns the raw audio data from any given audio input stream.
	 * @param audioIS
	 * @return Raw audio data.
	 * @throws IOException
	 */
	public static byte[] GetAudioData(AudioInputStream audioIS) throws IOException {
		byte[] data = null;
		ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream(); // The stream that collects all the audio byte data
		 
		/*
		 * Reads all the bytes from the audio files.
		 * It can only read a certain length of bytes at a time, hence the intermediate buffer.
		 */
		byte[] intermediateBuffer = new byte[INTERMEDIATE_BUFFER_STD_LENGTH];
		int numberOfBytesRead;
		while((numberOfBytesRead = audioIS.read(intermediateBuffer, 0, intermediateBuffer.length)) != -1){
			byteArrayOS.write(intermediateBuffer, 0, numberOfBytesRead);
		}
		byteArrayOS.close();
		
		data = byteArrayOS.toByteArray();	// Gets total number of bytes in the audio file.
		return data;
	}
	
	/**
	 * @description Creates an input stream for the given audio data.
	 * @return An audio input stream.
	 */
	public AudioInputStream getInputStream() {
		if(audioData == null)
			return null; // If no audioData has been pulled yet, no input stream can be created.
		ByteArrayInputStream byteArrayIS = new ByteArrayInputStream(audioData);
		return new AudioInputStream(byteArrayIS, format, audioData.length);
	}
	
	/**
	 * @description Obtains the frame size in bytes.
	 * @return The frame size in bytes.
	 */
	public int getFrameSize() {
		return format.getFrameSize();
	}
	
	/**
	 * @description Gets the frame rate for the given audio (i.e. frames per second).
	 * @return The frame rate.
	 */
	public float getFrameRate() {
		return format.getFrameRate();
	}
	
	/**
	 * @description Gets the duration of the audio data
	 * @return Duration of audio data
	 */
	public double getDuration() {
		return (getInputStream().getFrameLength() + 0.0) / (getFrameSize() * getFrameRate());
	}
	
	/**
	 * @description Saves the audio data to any given destination.
	 * @param destination The file destination for where the audio data will be saved.
	 * @throws IOException
	 * @throws UnsupportedAudioFileException
	 */
	public void saveAudio(File destination) throws IOException, UnsupportedAudioFileException {
		if(audioData == null)
			return; // No data to write to file.
		
		AudioInputStream audioIS = getInputStream();
		
		AudioSystem.write(audioIS, AudioFileFormat.Type.WAVE, destination);
	}
}
