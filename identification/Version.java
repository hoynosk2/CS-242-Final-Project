package identification;

public class Version {
	public static final double VERSION_NUM = 0.1;
	public static final String VERSION_STR = "v." + VERSION_NUM;
	public static final String VERSION_DATE = "Nov. 21st, 2017";
	public static final String[] authors = 
	{
			"Dan Hoynoski"
	};
	
	
	public static String GetAuthors() {
		StringBuilder builder = new StringBuilder();
		for(String next : authors) 
			builder.append(next + System.lineSeparator());
		return builder.toString();
	}
}
