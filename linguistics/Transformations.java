package linguistics;

import java.sql.SQLException;

import tools.mysql.DatabaseAccess;

public class Transformations {
	
	/**
	 * @description applies transformations in a very particular ordering.
	 * @param access The database access object to retrieve information on symbols.
	 * @param word The word to transform.
	 * @return A fully transformed word.
	 * @throws SQLException
	 */
	public static KoreanSymbol[] Transform(DatabaseAccess access, KoreanSymbol[] word) throws SQLException {
		word = Resyllabification(access, word);
		word = SyllableFinalClosureUnrelease(access, word);
		word = NasalAssimilation(access, word);
		word = NiunToRiulAssimulation(access, word);
		
		return word;
	}
	
	/**
	 * @description If 'ㅇ' is the beginning consonant in the next symbol and the previous ends
	 * in a consonant, then this consonant "travels over" and takes the place of 'ㅇ'.
	 * @param access Database access object to retrieve information on symbols.
	 * @param block The Korean word to transform.
	 * @return A transformed Korean word.
	 * @throws SQLException 
	 * @note Original word is not modified.
	 */
	public static KoreanSymbol[] Resyllabification(DatabaseAccess access, KoreanSymbol[] block) throws SQLException {
		KoreanSymbol[] transformed = new KoreanSymbol[block.length];
		
		if(block.length == 1) {
			transformed[0] = KoreanSymbol.GetDeepCopy(block[0]);
			return transformed;
		}
		
		for(int i = 0; i < block.length - 1; i++) {
			KoreanSymbol symbol1 = block[i];
			KoreanSymbol symbol2 = block[i+1];
			
			String[] strokes1 = symbol1.getStrokeOrder();
			String[] strokes2 = symbol2.getStrokeOrder();
			
			if(strokes2[0].equals(KoreanSymbol.이응) && 
			   strokes1[strokes1.length - 1].toCharArray()[0] >= KoreanSymbol.HANGUL_COMPATIBILITY_JAMO_CONSONANT_RANGE_LOW &&
			   strokes1[strokes1.length - 1].toCharArray()[0] <= KoreanSymbol.HANGUL_COMPATIBILITY_JAMO_CONSONANT_RANGE_HIGH) 
			{
				// (src, src-offset, dest, offset, count)
				String[] modifiedStrokes1 = new String[strokes1.length - 1]; // Removing last character.
				System.arraycopy(strokes1, 0, modifiedStrokes1, 0, strokes1.length - 1);
				transformed[i] = access.getSymbol(modifiedStrokes1);
				
				String[] modifiedStrokes2 = new String[strokes2.length]; // Removing last character.
				System.arraycopy(strokes2, 1, modifiedStrokes2, 1, strokes2.length - 1);
				modifiedStrokes2[0] = strokes1[strokes1.length - 1]; 		// Move final character from symbol1 to starting location of symbol2.
				transformed[i+1] = access.getSymbol(modifiedStrokes2);
			}else {
				transformed[i] = KoreanSymbol.GetDeepCopy(symbol1);
				transformed[i+1] = KoreanSymbol.GetDeepCopy(symbol2);
			}
				
		}
		return transformed;
	}
	/**
	 * @description At the end of a word or before a consonant, all Korean consonants are pronounced with closure of the speech organs involved,
	 * that is, without releasing air. As a result, sound changes occur in consonants in word-final or pre-consonantal position.
	 * @param access Database access object to retrieve information on symbols.
	 * @param word The Korean word to transform.
	 * @return A transformed Korean word.
	 * @throws SQLException
	 * @note Original word is not modified.
	 */
	public static KoreanSymbol[] SyllableFinalClosureUnrelease(DatabaseAccess access, KoreanSymbol[] word) throws SQLException {
		KoreanSymbol[] transformed = new KoreanSymbol[word.length];
		for(int i = 0; i < word.length - 1; i++) {
			KoreanSymbol symbol1 = word[i];
			KoreanSymbol symbol2 = word[i+1];
			
			String[] strokes1 = symbol1.getStrokeOrder();
			String[] strokes2 = symbol2.getStrokeOrder();
			
			if(_IsConsonant(strokes2[0]) && !_EndsInUniqueDoubleConsonant(strokes1)) {
				String modifiedLetter = _SyllableFinalClosureUnreleaseHelper(strokes1[strokes1.length - 1]);
				strokes1[strokes1.length - 1] = modifiedLetter;
			}
			
			transformed[i] = access.getSymbol(strokes1);
			transformed[i+1] = KoreanSymbol.GetDeepCopy(symbol2);
		}
		
		// Final symbol is checked if ends in consonant.
		String[] strokes = word[word.length - 1].getStrokeOrder();
		String modifiedLetter = _SyllableFinalClosureUnreleaseHelper(strokes[strokes.length - 1]);
		strokes[strokes.length - 1] = modifiedLetter;
		
		transformed[transformed.length - 1] = access.getSymbol(strokes);
		
		return transformed;
	}
	
	/**
	 * @description A mapping helper function for SyllableFinalClosureUnrelease
	 * @param letter The letter to try and map to another letter.
	 * @return The mapped letter.
	 */
	private static String _SyllableFinalClosureUnreleaseHelper(String letter) {
		if(letter.equals(KoreanSymbol.피읖)) {
			return KoreanSymbol.비읍;
		} else if(letter.equals(KoreanSymbol.티읕) || 
				  letter.equals(KoreanSymbol.시옷) ||
				  letter.equals(KoreanSymbol.쌍시옷) ||
				  letter.equals(KoreanSymbol.지읒) ||
				  letter.equals(KoreanSymbol.치읓))
		{
			return KoreanSymbol.디귿;
		} else if(letter.equals(KoreanSymbol.기역) || 
				  letter.equals(KoreanSymbol.키읔) || 
				  letter.equals(KoreanSymbol.쌍기역)) 
		{
			return KoreanSymbol.기역;
		} else 
			return letter; // Unmodified letter.
	}
	
	/**
	 * @description All plosive and fricative consonants become the corresponding nasal consonants before a nasal consonant (ㅁ,ㄴ).
	 * @param access Database access object to retrieve information on symbols.
	 * @param word The Korean word to transform.
	 * @return A transformed Korean word.
	 * @throws SQLException
	 * @note Original word is not modified.
	 */
	public static KoreanSymbol[] NasalAssimilation(DatabaseAccess access, KoreanSymbol[] word) throws SQLException {
		KoreanSymbol[] transformed = new KoreanSymbol[word.length];
		
		if(word.length == 1) {
			transformed[0] = KoreanSymbol.GetDeepCopy(word[0]);
			return transformed;
		}
		
		for(int i = 0; i < word.length - 1; i++) {
			KoreanSymbol symbol1 = word[i];
			KoreanSymbol symbol2 = word[i+1];
			
			String[] strokes1 = symbol1.getStrokeOrder();
			String[] strokes2 = symbol2.getStrokeOrder();
			
			if(strokes2[0].equals(KoreanSymbol.미음)) {
				if(strokes1[strokes1.length - 1].equals(KoreanSymbol.비읍) ||
				   strokes1[strokes1.length - 1].equals(KoreanSymbol.피읖)) 
				{
					strokes1[strokes1.length - 1] = KoreanSymbol.미음;
				} else if(strokes1[strokes1.length - 1].equals(KoreanSymbol.디귿) || 
						  strokes1[strokes1.length - 1].equals(KoreanSymbol.티읕) ||
						  strokes1[strokes1.length - 1].equals(KoreanSymbol.시옷) ||
						  strokes1[strokes1.length - 1].equals(KoreanSymbol.쌍시옷) ||
						  strokes1[strokes1.length - 1].equals(KoreanSymbol.지읒) ||
						  strokes1[strokes1.length - 1].equals(KoreanSymbol.치읓) ||
						  strokes1[strokes1.length - 1].equals(KoreanSymbol.히읗))
				{
					strokes1[strokes1.length - 1] = KoreanSymbol.니은;
				} else if(strokes1[strokes1.length - 1].equals(KoreanSymbol.기역) ||
						  strokes1[strokes1.length - 1].equals(KoreanSymbol.키읔) ||
						  strokes1[strokes1.length - 1].equals(KoreanSymbol.쌍기역))
				{
					strokes1[strokes1.length - 1] = KoreanSymbol.이응;
				}
			}
			transformed[i] = access.getSymbol(strokes1);
			transformed[i+1] = KoreanSymbol.GetDeepCopy(symbol2);
		}
		return transformed;
	}
	
	/**
	 * @description When ㄹ and ㄴ come together, the ㄴ sound is usually replaced by the ㄹ sound.
	 * @param access Database access object to retrieve information on symbols.
	 * @param word The Korean word to transform.
	 * @return A transformed Korean word.
	 * @throws SQLException
	 * @note Original word is not modified.
	 */
	public static KoreanSymbol[] NiunToRiulAssimulation(DatabaseAccess access, KoreanSymbol[] word) throws SQLException {
		KoreanSymbol[] transformed = new KoreanSymbol[word.length];
		
		if(word.length == 1) {
			transformed[0] = KoreanSymbol.GetDeepCopy(word[0]);
			return transformed;
		}
		
		for(int i = 0; i < word.length - 1; i++) {
			KoreanSymbol symbol1 = word[i];
			KoreanSymbol symbol2 = word[i+1];
			
			String[] strokes1 = symbol1.getStrokeOrder();
			String[] strokes2 = symbol2.getStrokeOrder();
			
			if(strokes1[strokes1.length - 1].equals(KoreanSymbol.리을) &&
			   strokes2[0].equals(KoreanSymbol.니은))
			{
				strokes2[0] = KoreanSymbol.리을;
			} else if(strokes1[strokes1.length - 1].equals(KoreanSymbol.니은) &&
					  strokes2[0].equals(KoreanSymbol.리을)) 
			{
				strokes1[strokes1.length - 1] = KoreanSymbol.리을;
			}
			
			transformed[i] = access.getSymbol(strokes1);
			transformed[i+1] = access.getSymbol(strokes2); // Potentially could slow down here.
		}
		return transformed;
	}
	
	/**
	 * @description Determines if the stroke ordering ends with a double consonant that is unique.
	 * @param strokes
	 * @return
	 * @note Strokes must contain at least two strokes within (i.e. impossible to form symbol with only one stroke)
	 */
	private static boolean _EndsInUniqueDoubleConsonant(String[] strokes) {
		if(_IsConsonant(strokes[strokes.length - 1]) && _IsConsonant(strokes[strokes.length - 2]))
			return true;
		return false;
	}
	
	/**
	 * @description Determines whether or not the given character letter is a consonant from the Korean alphabet.
	 * @param letter The letter to check.
	 * @return True if the letter is a consonant, otherwise false (e.g. if is a vowel).
	 */
	private static boolean _IsConsonant(String letter) {
		if(letter.toCharArray()[0] >= KoreanSymbol.HANGUL_COMPATIBILITY_JAMO_CONSONANT_RANGE_LOW &&
		   letter.toCharArray()[0] <= KoreanSymbol.HANGUL_COMPATIBILITY_JAMO_CONSONANT_RANGE_HIGH)
		{
			return true;
		} else
			return false;
	}
}
