package linguistics;

public class SpecialSymbols {
	public static final int NATIVE_KOREAN_NUMBERS = 0;		// Sole-Korean created numbering system.
    public static final int SINO_KOREAN_NUMBERS = 1;			// Chinese derived numbering system.
	
	private static final String[] baseSinoNumbers = 
    {
        "영/공",      // zero
        "일",        // one
        "이",        // two
        "삼",        // three
        "사",        // four
        "오",        // five
        "육",        // six
        "칠",        // seven
        "팔",        // eight
        "구",        // nine
    };
    private static final String[] baseSinoPrefixes = 
    {
        "십",        // ten
        "백",        // 100
        "천",        // thousand
        "만",        // ten-thousand
        "억",        // hundred-thousand
        "조",        // trillion
        "경",        // ten-quadrillion
        "해"         // hundred-quintillion
    };
    private static final String[] baseNativeNumbers = 
    {
        "No Zero",  // No native korean for zero
        "하나",       // one
        "둘",        // two
        "셋",        // three
        "넷",        // four
        "다섯",       // five
        "여섯",       // six
        "일곱",       // seven
        "여덟",       // eight
        "아홉",       // nine
    };
    private static final String[] baseNativeTensPrefixes =
    {
        "열",         // ten
        "스물",       // twenty
        "서른",       // thirty
        "마흔",       // fourty
        "쉰",         // fifty
        "예순",       // sixty
        "일흔",       // seventy
        "여든",       // eighty
        "아흔"        // ninty
    };
    
    private static final int SUBJECT = 0; 	// Word is a subject.
    private static final int OBJECT = 1; 	// Word is an object.
    private static final int NEITHER = 2;  	// Word is neither subject or object.
    
    /**
     * Generates a random integer between the low and high ranges.
     * @param low The low range.
     * @param high The high range.
     * @return A random integer.
     */
    private static int _GenerateRandomInt(int low, int high) {
    		return (int)Math.floor(Math.random() * (high - low)) + low;
    }
    
    /**
     * @description Generated a 1 to 3 symbol word in Korean. Obviously, the words are gibberish.
     * @param type Whether or not the word is an object, subject, or neither.
     * @return A Korean word string.
     */
    private static String _GenerateRandomKoreanWord(int type) {
    		String word = "";
    		int numberOfSymbols = _GenerateRandomInt(1, 3);
    		int finalConsonantDeterminer = 0;
    		for(int i = 0; i < numberOfSymbols; i++) {
    			int begin = _GenerateRandomInt(0, 19) * 0x24C; 	// Hops to the next alphabetically correct consonant letter in starting location.
    			int middle = _GenerateRandomInt(0, 21) * 0x1C; 	// Hops to the next alphabetically correct vowel letter in second location.
    			finalConsonantDeterminer = _GenerateRandomInt(0, 28); 			// Varies the final consonant location.
    			
    			int rawHangul = begin + middle + finalConsonantDeterminer + 0xAC00;
    			word = word + Character.toChars(rawHangul)[0];
    		}
    		if(type == SUBJECT) {
    			//Subject
    			word += (finalConsonantDeterminer > 0) ? '는' : '은';
    		} else if(type == OBJECT){
    			//Object
    			word += (finalConsonantDeterminer > 0) ? '를' : '을';
    		}
    		return word;
    }
    
    /**
     * @description Generates a random Korean sentence consisting of a subject, object, and verb.
     * @return A random Korean sentence.
     */
    public static String GenerateRandomKoreanSentence() {
    		return _GenerateRandomKoreanWord(SUBJECT) + " " + _GenerateRandomKoreanWord(OBJECT) + " " + _GenerateRandomKoreanWord(NEITHER) + "습니다";
    }
    
    /**
     * @description Generates a Korean number text from an integer.
     * @param next The number to generate.
     * @param type Either using the Sino Korean number system or the Native Korean number system.
     * @return
     */
	public static String CreateKoreanNumberText(int next, int type){
        StringBuilder number = new StringBuilder(Integer.toString(next)).reverse();     // Reverse b/c easer to start on low end of the string 0, 1, 2, 3,... etc.
        if(type == NATIVE_KOREAN_NUMBERS){
            int nextNumber = Integer.parseInt(number.substring(0, 1));
            if(number.length() == 1 && nextNumber == 0)
                return baseSinoNumbers[0];
            String nextKor = nextNumber != 0 ? baseNativeNumbers[nextNumber] : ""; 
            if(next > 9){
                nextNumber = Integer.parseInt(number.substring(1, 2));
                nextKor = baseNativeTensPrefixes[nextNumber - 1] + nextKor;
                return nextKor;
            }
            return nextKor;
        }else if(type == SINO_KOREAN_NUMBERS){
            StringBuilder koreanText = new StringBuilder("");
            
            // Because there are two representations for "zero" this special case is used. 
            if(number.length() == 1 && Integer.parseInt(number.substring(0, 1)) == 0)
                return baseSinoNumbers[0];
            
            int oneTimePrefix = 3;  // Starts with 만.
            int prefixCounter = 5;  // For 십, 백, & 천
            for(int i = 0; i < number.length() && oneTimePrefix < baseSinoPrefixes.length; i++, prefixCounter--){
                int nextNumber = Integer.parseInt(number.substring(i, i + 1));			// Gets the next number.
                String nextKor = nextNumber != 0 ? baseSinoNumbers[nextNumber] : "";      // Next Korean number (Do not include zero)
                if(prefixCounter == 0)
                    prefixCounter = 4;
                if(i == 0)
                    koreanText.append(nextKor);                                           // Base case.
                else{
                    if(nextKor.equals(baseSinoNumbers[1]))
                        nextKor = ""; 
                    switch(prefixCounter){
                        case 4:
                            //십 case
                            if(nextNumber != 0)
                                nextKor = baseSinoPrefixes[0] + nextKor;                  // backwards because of reverse.
                            else
                                continue;
                            break;
                        case 3:
                            //백 case
                            if(nextNumber != 0)
                                 nextKor = baseSinoPrefixes[1] + nextKor;
                            else
                                continue;
                            break;
                        case 2:
                            //천 case
                            if(nextNumber != 0)
                                nextKor = baseSinoPrefixes[2] + nextKor;
                            else
                                continue;
                            break;
                        case 1:
                            //New "oneTimePrefex"
                            nextKor = baseSinoPrefixes[oneTimePrefix] + nextKor;
                            oneTimePrefix++;
                        default:
                    }
                    koreanText.append(nextKor);
                }
            }
            return koreanText.reverse().toString();
        }
        return null;
    }
}
