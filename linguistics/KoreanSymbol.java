package linguistics;

/**
 * @description Defines an immutable Korean Symbol.
 * @author dan
 *
 */
public final class KoreanSymbol {
	
	public static final Integer UNDEFINED_GOV_FREQ = null;
	
	/* Korean Consonant Alphabet */ 
	public static final String 기역 = "ㄱ";
	public static final String 쌍기역 = "ㄲ";
	public static final String 니은 = "ㄴ";
	public static final String 디귿 = "ㄷ";
	public static final String 쌍디귿 = "ㄸ";
	public static final String 리을 = "ㄹ";
	public static final String 미음 = "ㅁ";
	public static final String 비읍 = "ㅂ";
	public static final String 쌍비읍 = "ㅃ";
	public static final String 시옷 = "ㅅ";
	public static final String 쌍시옷 = "ㅆ";
	public static final String 이응 = "ㅇ";
	public static final String 지읒 = "ㅈ";
	public static final String 쌍지읒 = "ㅉ";
	public static final String 치읓 = "ㅊ";
	public static final String 키읔 = "ㅋ";
	public static final String 티읕 = "ㅌ";
	public static final String 피읖 = "ㅍ";
	public static final String 히읗 = "ㅎ";
	
	
	public static final int HANGUL_CHOSEONG_RANGE_LOW = 0x1100;
	public static final int HANGUL_CHOSEONG_RANGE_HIGH = 0x1112;
	public static final int HANGUL_JONGSEONG_RANGE_LOW = 0x11A8;
	public static final int HANGUL_JONGSEONG_RANGE_HIGH = 0x11C2;
	
	public static final int HANGUL_COMPATIBILITY_JAMO_CONSONANT_RANGE_LOW = 0x3131;
	public static final int HANGUL_COMPATIBILITY_JAMO_CONSONANT_RANGE_HIGH = 0x314e;
	public static final int HANGUL_COMPATIBILITY_JAMO_VOWEL_RANGE_LOW = 0x314f;
	public static final int HANGUL_COMPATIBILITY_JAMO_VOWEL_RANGE_HIGH = 0x3163;
	
	private final String symbol;
	private final String[] strokeOrder;
	private final Integer govFreq;
	private final String audioAddress;
	
	/**
	 * @description Defines a Korean symbol and its parts.
	 * @param symbol The Korean symbol at hand.
	 * @param strokeOrder The stroke ordering of the Korean symbol (i.e. how it is written in order).
	 * @param govFreq The government provided frequency of how common the symbol is in daily usage.
	 */
	public KoreanSymbol(final String symbol, final String[] strokeOrder, final int govFreq) {
		this.symbol = symbol;
		this.strokeOrder = strokeOrder;
		this.govFreq = new Integer(govFreq);
		this.audioAddress = null;
	}
	
	/**
	 * @description Defines a Korean symbol and its parts.
	 * @param symbol The Korean symbol at hand.
	 * @param strokeOrder The stroke ordering of the Korean symbol (i.e. how it is written in order).
	 * @param govFreq The government provided frequency of how common the symbol is in daily usage.
	 * @param audioAddress The audio wave file address on the system for symbol.
	 */
	public KoreanSymbol(final String symbol, final String[] strokeOrder, final int govFreq, final String audioAddress) {
		this.symbol = symbol;
		this.strokeOrder = strokeOrder;
		this.govFreq = new Integer(govFreq);
		this.audioAddress = audioAddress;
	}
	
	/**
	 * @description Defines a Korean symbol and its parts.
	 * @param symbol The Korean symbol at hand.
	 * @param strokeOrder The stroke ordering of the Korean symbol (i.e. how it is written in order).
	 * @param govFreq The government provided frequency of how common the symbol is in daily usage.
	 */
	public KoreanSymbol(final String symbol, final String[] strokeOrder, final Integer govFreq) {
		this.symbol = symbol;
		this.strokeOrder = strokeOrder;
		this.govFreq = govFreq;
		this.audioAddress = null;
	}
	
	/**
	 * @description Defines a Korean symbol and its parts.
	 * @param symbol The Korean symbol at hand.
	 * @param strokeOrder The stroke ordering of the Korean symbol (i.e. how it is written in order).
	 * @param govFreq The government provided frequency of how common the symbol is in daily usage.
	 * @param audioAddress The audio wave file address on the system for symbol.
	 */
	public KoreanSymbol(final String symbol, final String[] strokeOrder, final Integer govFreq, final String audioAddress) {
		this.symbol = symbol;
		this.strokeOrder = strokeOrder;
		this.govFreq = govFreq;
		this.audioAddress = audioAddress;
	}
	
	/**
	 * @description Defines a Korean symbol and its parts.
	 * @param symbol The Korean symbol at hand.
	 * @param strokeOrder The stroke ordering of the Korean symbol (i.e. how it is written in order).
	 */
	public KoreanSymbol(final String symbol, final String[] strokeOrder) {
		this.symbol = symbol;
		this.strokeOrder = strokeOrder;
		this.govFreq = UNDEFINED_GOV_FREQ;
		this.audioAddress = null;
	}
	
	/**
	 * @description Gets the audio file's address for the symbol, if provided originally.
	 * @return The audio file address's location on the file system.
	 */
	public String getAudioAddress() {
		if(audioAddress != null)
			return new String(audioAddress);
		return null;
	}
	
	/**
	 * @description Gets the Korean symbol at hand.
	 * @return The main Korean symbol
	 */
	public String getSymbol() {
		if(symbol != null)
			return new String(symbol);
		return null;
	}
	
	/**
	 * @decription Gets the stroke ordering array for the Korean symbol. Details the writing order of letters to make symbol.
	 * @return Stroke ordering array.
	 */
	public String[] getStrokeOrder() {
		String[] strokeOrderCpy = new String[strokeOrder.length];
		System.arraycopy(strokeOrder, 0, strokeOrderCpy, 0, strokeOrder.length); 
		return strokeOrder;
	}
	
	/**
	 * @description Gets the government provided frequency of symbol in daily usage.
	 * @return government frequency of the symbol.
	 */
	public Integer getGovernmentFrequency() {
		if(govFreq != null)
			return new Integer(govFreq);
		return null;
	}
	
	/**
	 * @description Gets a deep copy of a Korean symbol.
	 * @param symbol The symbol to copy.
	 * @return A deep copy of the provided symbol.
	 */
	public static KoreanSymbol GetDeepCopy(KoreanSymbol symbol) {
		return new KoreanSymbol(symbol.getSymbol(), symbol.getStrokeOrder(), symbol.getGovernmentFrequency(), symbol.getAudioAddress());
	}
	
	/**
	 * @description Compares to see if two Korean symbols are the same.
	 * @param o The potential Korean symbol to compare against.
	 * @return True if the object provided is the same as this korean symbol, otherwise false.
	 */
	public boolean equals(Object o) {
		if(o == this)
			return true;
		
		// Must check if 'o' is event a KoreanSymbol object before cast is done.
		if(!(o  instanceof KoreanSymbol))
			return false;
		
		if(((KoreanSymbol)o).getSymbol().equals(getSymbol()))
			return true;
		return false;
	}
	
	/**
	 * @description Parses a Korean sentence/paragraph from a list of Korean symbols.
	 * @param string The Korean sentence/paragraph.
	 * @return A fully concatenated Korean sentence/paragraph.
	 */
	public static String ParseString(KoreanSymbol[] string) {
		StringBuilder builder = new StringBuilder();
		for(KoreanSymbol next : string) {
			builder.append(next.getSymbol());
		}
		return builder.toString();
	}
	
	/**
	 * @description Gives textual representation of the immutable Korean symbol.
	 * @return A textual representation of the symbol and its parts.
	 */
	public String toString() {
		return symbol + " [" + String.join(",", strokeOrder) + "] @ " + govFreq;
	}
}
