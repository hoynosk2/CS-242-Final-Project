package tests;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.Test;

import linguistics.KoreanSymbol;
import tools.mysql.DatabaseAccess;

public class DatabaseAccessTest {
	
	private static final KoreanSymbol kSymbol = new KoreanSymbol("_녕", new String[] {"ㄴ", "ㅕ", "ㅇ"}, 999999999);
	private static final KoreanSymbol kSymbol2 = new KoreanSymbol("_안", new String[] {"ㅇ", "ㅏ", "ㄴ"}, 999999998);
	private static final KoreanSymbol kSymbol3 = new KoreanSymbol("만", new String[] {"ㅁ", "ㅏ", "ㄴ"}, 44672);
	@Test
	public void testOpenCloseDBConnection() {
		DatabaseAccess dba = new DatabaseAccess(DatabaseAccess.MASTER_USER);
		try {
			dba.open();
			dba.close();
			assertTrue(true); 
		} catch (SQLException e) {
			fail("Failed to open and close database connection");
		}
	}
	
	@Test
	public void testAddRemoveSymbolEntry() {
		DatabaseAccess dba = new DatabaseAccess(DatabaseAccess.MASTER_USER);
		try {
			dba.open();
			boolean add = dba.addSymbolEntry(kSymbol);
			boolean removed = dba.removeSymbolEntry(kSymbol);
			dba.close();
			assertEquals(add, removed);
		}catch (SQLException e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}
	
	@Test
	public void testRemoveSymbol() {
		DatabaseAccess dba = new DatabaseAccess(DatabaseAccess.MASTER_USER);
		try {
			dba.open();
			boolean removed = dba.removeSymbolEntry(kSymbol);
			dba.close();
			assertTrue(!removed);
		}catch (SQLException e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}
	
	@Test
	public void testGetSymbol() {
		DatabaseAccess dba = new DatabaseAccess(DatabaseAccess.MASTER_USER);
		String answer = "가 [ㄱ,ㅏ] @ 150918";
		try {
			dba.open();
			KoreanSymbol nextSymbol = dba.getSymbol("가");
			dba.close();
			assertEquals(answer, nextSymbol.toString());
		}catch (SQLException e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}
	
	@Test
	public void testGetNextFrequentSymbolsWithoutAudio() {
		DatabaseAccess dba = new DatabaseAccess(DatabaseAccess.MASTER_USER);
		String[] answer = 
		{
				kSymbol.getSymbol(),
				kSymbol2.getSymbol()
		};
		try {
			dba.open();
			dba.addSymbolEntry(kSymbol);
			dba.addSymbolEntry(kSymbol2);
			KoreanSymbol[] symbols = dba.getNextFrequentSymbolsWithoutAudio();
			String[] result = {symbols[0].getSymbol(), symbols[1].getSymbol()};
			dba.removeSymbolEntry(kSymbol);
			dba.removeSymbolEntry(kSymbol2);
			dba.close();
			assertArrayEquals(answer, result);
		}catch (SQLException e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}
	
	@Test
	public void testGetSymbolThroughStrokes() {
		DatabaseAccess dba = new DatabaseAccess(DatabaseAccess.MASTER_USER);
		try {
			dba.open();
			KoreanSymbol result = dba.getSymbol(kSymbol3.getStrokeOrder());
			dba.close();
			assertEquals(result.toString(), kSymbol3.toString());
		}catch (SQLException e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}
}
