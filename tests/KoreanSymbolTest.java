package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import linguistics.KoreanSymbol;

public class KoreanSymbolTest {
	
	public static KoreanSymbol kSymbol = new KoreanSymbol("만", new String[] {"ㅁ", "ㅏ", "ㄴ"}, 44672);
	
	@Test
	public void testGetSymbole() {
		String answer = "만";
		assertEquals(kSymbol.getSymbol(), answer);
	}
	
	@Test
	public void testGetGovernmentFrequency() {
		Integer answer = new Integer(44672);
		assertEquals(answer, kSymbol.getGovernmentFrequency());
	}
	
	@Test
	public void testDeepCopy() {
		KoreanSymbol deepCopy = KoreanSymbol.GetDeepCopy(kSymbol);
		assertFalse(deepCopy == kSymbol); // Verifies that they are not both not in the same memory location.
	}
}
