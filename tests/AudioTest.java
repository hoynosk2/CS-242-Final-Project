package tests;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.junit.Test;

import audio.AudioData;
import audio.AudioManipulation;

public class AudioTest {
	
	/**
	 * @description Tests the removal of silence segments located in the beginning and ending of an audio file.
	 */
	@Test
	public void removeSilenceTest() {
		/*
		 * Audio File 1 Specifications:
		 * Encoding - PCM SIGNED
		 * Sample Rate - 44100.0 Hz
		 * Sample Size - 16 bits
		 * Channels - 1 (mono) 
		 * Frame Size - 2 frames
		 * Frame Rate - 44100.0 Hz
		 * Big-Endian - False (WAV files are Little-Endian)
		 */
		AudioFormat format = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, (float)44100.0, 16, 1, 2, (float)44100.0, false);
		byte[] initialRawData = null, correctlyRemovedSilence = null;
		try {
			initialRawData = _getData(System.getProperty("user.dir") + "/src/audio/initialAudio1.txt"); 					// Unedited audio file 1 raw audio data
			correctlyRemovedSilence = _getData(System.getProperty("user.dir") + "/src/audio/removeSilenceAudio1.txt"); 	// Expected raw data output
		}catch(IOException ioe) {	
			fail("IOException was thrown when removing silence.");
		}
		try { 
			/*
			 * Run the 'RemoveSilence' operation and compare with correct output.
			 */
			AudioData audio = new AudioData(initialRawData, format);
			audio.getAudioData();
			
			//audio.saveAudio(new File("/Users/dan/Desktop/beforeRemove.wav"));
			AudioManipulation.RemoveSilence(audio, AudioManipulation.DEFAULT_SILENCE_BUFFER);
			//audio.saveAudio(new File("/Users/dan/Desktop/afterRemove.wav"));
			
			byte[] result = audio.getAudioData();
			assertTrue(Arrays.equals(result, correctlyRemovedSilence));
		}catch(IOException ioe) {
			fail("IOException was thrown when removing silence.");
		}catch (UnsupportedAudioFileException e) {
			fail("UnsupportedAudioFileException was thrown when removing silence.");
		}
		 
	}
	
	/**
	 * @description Combining of two audio source functionality test.
	 */
	@Test
	public void combineAudioTest() {
		/*
		 * Audio File 2 Specifications:
		 * Encoding - PCM SIGNED
		 * Sample Rate - 44100.0 Hz
		 * Sample Size - 16 bits
		 * Channels - 1 (mono)
		 * Frame Size - 2 frames
		 * Frame Rate - 44100.0 Hz
		 * Big-Endian - False (WAV files are Little-Endian)
		 */
		AudioFormat format = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, (float)44100.0, 16, 1, 2, (float)44100.0, false);
		byte[] initialRawData = null, correctlyCombinedAudio = null;
		try {
			initialRawData = _getData(System.getProperty("user.dir") + "/src/audio/initialAudio2.txt"); 				// Unedited audio file 2 raw audio data
			correctlyCombinedAudio = _getData(System.getProperty("user.dir") + "/src/audio/combinedAudioAudio2.txt"); 	// Expected edited audio data output
		}catch(IOException ioe) {
			fail("IOException was thrown when combining audio.");
		}
		
		try {
			/*
			 * Run the 'CombineAudio' operation on the same audio source to get a single loop of audio.
			 */
			AudioData audio = new AudioData(initialRawData, format);
			audio.getAudioData();
			
			//audio.saveAudio(new File("/Users/dan/Desktop/beforeCombine.wav"));
			audio = AudioManipulation.CombineAudio(audio, audio);
			//audio.saveAudio(new File("/Users/dan/Desktop/afterCombine.wav"));
			
			byte[] result = audio.getAudioData();
			assertTrue(Arrays.equals(result, correctlyCombinedAudio));
		}catch(IOException ioe) {
			fail("IOException was thrown when combining audio.");
		}catch (UnsupportedAudioFileException e) {
			fail("UnsupportedAudioFileException was thrown when combining audio.");
		}
	}
	
	/**
	 * @description Fading the audio in test during the first few seconds of audio.
	 */
	@Test
	public void fadeAudioInTest() {
		/*
		 * Audio File 2 Specifications:
		 * Encoding - PCM SIGNED
		 * Sample Rate - 44100.0 Hz
		 * Sample Size - 16 bits
		 * Channels - 1 (mono)
		 * Frame Size - 2 frames
		 * Frame Rate - 44100.0 Hz
		 * Big-Endian - False (WAV files are Little-Endian)
		 */
		AudioFormat format = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, (float)44100.0, 16, 1, 2, (float)44100.0, false);
		byte[] initialRawData = null, correctlyFadeInAudio = null;
		try {
			initialRawData = _getData(System.getProperty("user.dir") + "/src/audio/initialAudio2.txt"); 			// Unedited audio file 2 raw audio data
			correctlyFadeInAudio = _getData(System.getProperty("user.dir") + "/src/audio/fadeAudioInAudio2.txt");	// Expected edited audio data output
		}catch(IOException ioe) {
			fail("IOException was thrown when fading audio in.");
		}
		
		try {
			/*
			 * Run the 'FadeAudio' operation in order to fade the audio in during the first 5 seconds.
			 */
			AudioData audio = new AudioData(initialRawData, format);
			audio.getAudioData();
			
			//audio.saveAudio(new File("/Users/dan/Desktop/beforeFadeIn.wav"));
			AudioManipulation.FadeAudio(audio, 5, AudioManipulation.FADE_IN);
			//audio.saveAudio(new File("/Users/dan/Desktop/afterFadeIn.wav"));
			
			byte[] result = audio.getAudioData();
			assertTrue(Arrays.equals(result, correctlyFadeInAudio));
		}catch(IOException ioe) {
			fail("IOException was thrown when fading audio in.");
		}catch (UnsupportedAudioFileException e) {
			fail("UnsupportedAudioFileException was thrown when fading audio in.");
		}
	}
	
	/**
	 * @description Fading the audio out test during the last few seconds of audio.
	 */
	@Test
	public void fadeAudioOutTest() {
		/*
		 * Audio File 2 Specifications:
		 * Encoding - PCM SIGNED
		 * Sample Rate - 44100.0 Hz
		 * Sample Size - 16 bits
		 * Channels - 1 (mono)
		 * Frame Size - 2 frames
		 * Frame Rate - 44100.0 Hz
		 * Big-Endian - False (WAV files are Little-Endian)
		 */
		AudioFormat format = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, (float)44100.0, 16, 1, 2, (float)44100.0, false);
		byte[] initialRawData = null, correctlyFadeOutAudio = null;
		try {
			initialRawData = _getData(System.getProperty("user.dir") + "/src/audio/initialAudio2.txt"); 				// Unedited audio file 2 raw audio data
			correctlyFadeOutAudio = _getData(System.getProperty("user.dir") + "/src/audio/fadeAudioOutAudio2.txt");	// Expected edited audio data output
		}catch(IOException ioe) {
			fail("IOException was thrown when fading out audio.");
		}
		
		try {
			/*
			 * Run the 'FadeAudio' operation in order to fade the audio out during the last 5 seconds.
			 */
			AudioData audio = new AudioData(initialRawData, format);
			audio.getAudioData();
			
			//audio.saveAudio(new File("/Users/dan/Desktop/beforeFadeOut.wav"));
			AudioManipulation.FadeAudio(audio, 5, AudioManipulation.FADE_OUT);
			//audio.saveAudio(new File("/Users/dan/Desktop/afterFadeOut.wav"));
			
			byte[] result = audio.getAudioData();
			assertTrue(Arrays.equals(result, correctlyFadeOutAudio));
		}catch(IOException ioe) {
			fail("IOException was thrown when fading out audio.");
		}catch (UnsupportedAudioFileException e) {
			fail("UnsupportedAudioFileException was thrown when fading out audio.");
		}
	}
	
	/**
	 * @description Change the total volume of the audio test.
	 */
	@Test
	public void volumeChangeTest() {
		/*
		 * Audio File 2 Specifications:
		 * Encoding - PCM SIGNED
		 * Sample Rate - 44100.0 Hz
		 * Sample Size - 16 bits
		 * Channels - 1 (mono)
		 * Frame Size - 2 frames
		 * Frame Rate - 44100.0 Hz
		 * Big-Endian - False (WAV files are Little-Endian)
		 */
		AudioFormat format = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, (float)44100.0, 16, 1, 2, (float)44100.0, false);
		byte[] initialRawData = null, correctlyChangeVolume = null;
		try {
			initialRawData = _getData(System.getProperty("user.dir") + "/src/audio/initialAudio2.txt");				// Unedited audio file 2 raw audio data
			correctlyChangeVolume = _getData(System.getProperty("user.dir") + "/src/audio/volumeChangeAudio2.txt");	// Expected edited audio data output
		}catch(IOException ioe) {
			fail("IOException was thrown when changing volume of audio.");
		}
		
		try {
			/*
			 * Run the 'ChangeVolume' operation in order to make the audio 5% as loud.
			 */
			AudioData audio = new AudioData(initialRawData, format);
			audio.getAudioData();
			
			//audio.saveAudio(new File("/Users/dan/Desktop/beforeVolumeChange.wav"));
			AudioManipulation.ChangeVolume(audio, 0.05);
			//audio.saveAudio(new File("/Users/dan/Desktop/afterVolumeChange.wav"));
			
			byte[] result = audio.getAudioData();
			assertTrue(Arrays.equals(result, correctlyChangeVolume));
		}catch(IOException ioe) {
			fail("IOException was thrown when changing volume of audio.");
		}catch (UnsupportedAudioFileException e) {
			fail("UnsupportedAudioFileException was thrown when changing volume of audio.");
		}
	}
	
	/**
	 * @description Test retrieval of frame rate from audio data source.
	 */
	@Test
	public void checkFrameRateTest() {
		/*
		 * Audio File 2 Specifications:
		 * Encoding - PCM SIGNED
		 * Sample Rate - 44100.0 Hz
		 * Sample Size - 16 bits
		 * Channels - 1 (mono)
		 * Frame Size - 2 frames
		 * Frame Rate - 44100.0 Hz
		 * Big-Endian - False (WAV files are Little-Endian)
		 */
		AudioFormat format = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, (float)44100.0, 16, 1, 2, (float)44100.0, false);
		byte[] initialRawData = null;
		try {
			initialRawData = _getData(System.getProperty("user.dir") + "/src/audio/initialAudio2.txt"); // Unedited audio file 2 raw audio data
		}catch(IOException ioe) {
			fail("IOException was thrown when checking frame rate");
		}
		AudioData audio = new AudioData(initialRawData, format);
		assertTrue((float)44100.0 == audio.getFrameRate());
	}
	
	/**
	 * @description Test retrieval of frame size from audio data source.
	 */
	@Test
	public void checkFrameSizeTest() {
		/*
		 * Audio File 2 Specifications:
		 * Encoding - PCM SIGNED
		 * Sample Rate - 44100.0 Hz
		 * Sample Size - 16 bits
		 * Channels - 1 (mono)
		 * Frame Size - 2 frames
		 * Frame Rate - 44100.0 Hz
		 * Big-Endian - False (WAV files are Little-Endian)
		 */
		AudioFormat format = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, (float)44100.0, 16, 1, 2, (float)44100.0, false);
		byte[] initialRawData = null;
		try {
			initialRawData = _getData(System.getProperty("user.dir") + "/src/audio/initialAudio2.txt"); // Unedited audio file 2 raw audio data
		}catch(IOException ioe) {
			fail("IOException was thrown when checking frame rate");
		}
		AudioData audio = new AudioData(initialRawData, format);
		assertTrue(2 == audio.getFrameSize());
	}
	
	/**
	 * @description Loads raw audio byte digits from text file and gives them compacted in byte array.
	 * @param path The path to where the text file lies.
	 * @return A byte array containing the raw audio data.
	 * @throws IOException
	 */
	private byte[] _getData(String path) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(path));
		List<Byte> bytes = new ArrayList<Byte>();
		try {
			String line = br.readLine(); // Each byte digit is seperate by a new line.
			
			/*
			 * Read each byte-line to collect all the audio data.
			 * Using an ArrayList as length is unknown.
			 */
		    while (line != null) {
		        bytes.add(new Byte(Byte.valueOf(line)));
		        line = br.readLine();
		    }
		    
		    /*
		     * Convert ArrayList of Byte Wrappers to an array of bytes.
		     */
		    byte[] data = new byte[bytes.size()];
			for(int i = 0; i < bytes.size(); i++) {
				data[i] = bytes.get(i);
			}
			return data;
		} finally {
		    br.close();
		}
	}

}
