package tests;

import static org.junit.Assert.*;

import java.sql.SQLException;

import org.junit.Test;

import linguistics.KoreanSymbol;
import linguistics.Transformations;
import tools.mysql.DatabaseAccess;

public class TransformationTests {

	@Test
	public void testResyllabification() {
		KoreanSymbol[] input = 
		{
				new KoreanSymbol("갇", new String[] {"ㄱ", "ㅏ", "ㄷ"}),
				new KoreanSymbol("없", new String[] {"ㅇ", "ㅓ", "ㅂ", "ㅅ"}),
		};
		KoreanSymbol[] answer = 
		{
				new KoreanSymbol("가", new String[] {"ㄱ", "ㅏ"}),
				new KoreanSymbol("덦", new String[] {"ㄷ", "ㅓ", "ㅂ", "ㅅ"}),
		};
		DatabaseAccess dba = new DatabaseAccess(DatabaseAccess.MASTER_USER);
		try {
			dba.open();
			KoreanSymbol[] transformed = Transformations.Resyllabification(dba, input);
			dba.close();
			assertEquals(KoreanSymbol.ParseString(transformed),KoreanSymbol.ParseString(answer));
		} catch (SQLException e) {
			fail("Failed to open and close database connection");
		}
	}
	
	@Test
	public void testSyllableFinalClosureUnrelease() {
		KoreanSymbol[] input = 
		{
				new KoreanSymbol("있", new String[] {"ㅇ", "ㅣ", "ㅆ"}),
				new KoreanSymbol("곺", new String[] {"ㄱ", "ㅗ", "ㅍ"}),
		};
		KoreanSymbol[] answer = 
		{
				new KoreanSymbol("읻", new String[] {"ㅇ", "ㅣ", "ㄷ"}),
				new KoreanSymbol("곱", new String[] {"ㄱ", "ㅗ", "ㅂ"}),
		};
		DatabaseAccess dba = new DatabaseAccess(DatabaseAccess.MASTER_USER);
		try {
			dba.open();
			KoreanSymbol[] transformed = Transformations.SyllableFinalClosureUnrelease(dba, input);
			dba.close();
			assertEquals(KoreanSymbol.ParseString(transformed),KoreanSymbol.ParseString(answer));
		} catch (SQLException e) {
			fail("Failed to open and close database connection");
		}
		
	}
	
	@Test
	public void testNasalAssimilation() {
		KoreanSymbol[] input = 
		{
				new KoreanSymbol("입", new String[] {"ㅇ", "ㅣ", "ㅂ"}),
				new KoreanSymbol("만", new String[] {"ㅁ", "ㅏ"}),
		};
		KoreanSymbol[] answer = 
		{
				new KoreanSymbol("임", new String[] {"ㅇ", "ㅣ", "ㅁ"}),
				new KoreanSymbol("만", new String[] {"ㅁ", "ㅏ", "ㄴ"}),
		};
		DatabaseAccess dba = new DatabaseAccess(DatabaseAccess.MASTER_USER);
		try {
			dba.open();
			KoreanSymbol[] transformed = Transformations.NasalAssimilation(dba, input);
			dba.close();
			assertEquals(KoreanSymbol.ParseString(transformed),KoreanSymbol.ParseString(answer));
		} catch (SQLException e) {
			fail("Failed to open and close database connection");
		}
	}
	
	@Test
	public void testNiunToRiulAssimulation() {
		KoreanSymbol[] input = 
		{
				new KoreanSymbol("진", new String[] {"ㅈ", "ㅣ", "ㄴ"}),
				new KoreanSymbol("리", new String[] {"ㄹ", "ㅣ"}),
		};
		KoreanSymbol[] answer = 
		{
				new KoreanSymbol("질", new String[] {"ㅈ", "ㅣ", "ㄹ"}),
				new KoreanSymbol("리", new String[] {"ㄹ", "ㅣ"}),
		};
		DatabaseAccess dba = new DatabaseAccess(DatabaseAccess.MASTER_USER);
		try {
			dba.open();
			KoreanSymbol[] transformed = Transformations.NiunToRiulAssimulation(dba, input);
			dba.close();
			assertEquals(KoreanSymbol.ParseString(transformed),KoreanSymbol.ParseString(answer));
		} catch (SQLException e) {
			fail("Failed to open and close database connection");
		}
	}

}
