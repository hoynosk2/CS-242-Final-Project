package tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ 

	LinguisticsTest.class,
	AudioTest.class,
	DatabaseAccessTest.class,
	LinguisticsTest.class,
	TransformationTests.class,
	AudioCaptureTest.class,
	KoreanSymbolTest.class
})

public class AllTests{ /* Nothing to see here */ } 
