package tests;

import static org.junit.Assert.*;

import java.io.IOException;

import javax.sound.sampled.LineUnavailableException;

import org.junit.Test;

import audio.AudioCapture;

public class AudioCaptureTest {
	@Test
	public void audioCaptureTest() {
		AudioCapture capture = new AudioCapture(2); // 6 seconds.
		capture.setRecordFileName("안");
		try {
			capture.start();
			while(capture.isRecording()) {
				//System.out.println(capture.isRecording());
			}
			boolean deleted = capture.getRecordFile().delete();
			assertTrue(deleted);
		} catch (LineUnavailableException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			assertTrue(false);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			assertTrue(false);
		} catch(SecurityException ex) {
			ex.printStackTrace();
			assertTrue(false);
		}
	}
}
