package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import linguistics.SpecialSymbols;

public class LinguisticsTest {

	@Test
	public void numberTest1() {
		assertTrue(SpecialSymbols.CreateKoreanNumberText(84832, SpecialSymbols.SINO_KOREAN_NUMBERS).equals("팔만사천팔백삼십이"));
	}
	
	@Test
	public void testRandomSentence() {
		String random = SpecialSymbols.GenerateRandomKoreanSentence().replaceAll("\\s+","");
		System.out.println(random);
		for(int i = 0; i < random.length(); i++) {
			int value = (int) random.toCharArray()[i];
			if(value < 0xAC00 || value > 0xD7A3)
				assertTrue(false);
		}
		assertTrue(true);
	}
} 
