package tools.mysql.entry.textcreator;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.sql.SQLException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;
import javax.swing.border.EtchedBorder;

import identification.Version;
import linguistics.KoreanSymbol;
import tools.Console;
import tools.mysql.DatabaseAccess;

public class ApplicationWindow {
	private JFrame frame;
	private JTextField symbol, strokeOrder, govFreq;
	private JButton add, quit;
	private DatabaseAccess dba;
	public ApplicationWindow(final String title) {
		this.frame = new JFrame(title + " " + Version.VERSION_STR);
		this.frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		this.frame.setResizable(false);
		this.setLook();
		dba = new DatabaseAccess(DatabaseAccess.MASTER_USER);
		this.frame.getContentPane().add(this._getMainContentPane(), BorderLayout.CENTER);
		_setActions();
		_initialize();
		this.frame.pack();
	}
	private void _initialize() {
		try {
			dba.open();
		} catch (SQLException e) {
			Console.simpleOutput(frame, "Database Connect Error", "Couldn't connect to DB.", true);
		}
	}
	private void _setActions() {
		quit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					dba.close();
				} catch(SQLException e1) {
					e1.printStackTrace();
				}
				frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
			}
		});
		add.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Integer govFreqObj = null;
				try {
					govFreqObj = Integer.parseInt(govFreq.getText());
				}catch(Exception ex) {
					govFreqObj = null;
				}
				KoreanSymbol kSymbol = new KoreanSymbol(symbol.getText(), strokeOrder.getText().split(","), govFreqObj);
				try {
					dba.addSymbolEntry(kSymbol);
					frame.setTitle("Added " + kSymbol.getSymbol());
				} catch (SQLException e1) {
					e1.printStackTrace();
				} finally {
					_clearText();
				}
			}
		});
	}
	private void _clearText() {
		symbol.setText("");
		strokeOrder.setText("");
		govFreq.setText("");
	}
	private JPanel _getMainContentPane() {
		JPanel main = new JPanel();
		main.setLayout(new GridBagLayout());
		main.setOpaque(true);
		main.setBackground(Color.WHITE);
		main.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
		
		symbol = new JTextField(2);
		main.add(symbol, new GridBagConstraints(	1 /* gridx */, 
												0 /* gridy */, 
												1 /* gridweight */, 
												1 /* gridheight */, 
												1.0 /* weightx */,
												1.0 /* weighty */,
												GridBagConstraints.LAST_LINE_START /* Anchor */,
												GridBagConstraints.HORIZONTAL /* fill */,
												new Insets(0,0,0,5),
												0 /* ipadx */,
												0 /* ipady*/ ));
		main.add(new JLabel("Symbol:"), new GridBagConstraints(	0 /* gridx */, 
																0 /* gridy */, 
																1 /* gridweight */, 
																1 /* gridheight */, 
																1.0 /* weightx */,
																1.0 /* weighty */,
																GridBagConstraints.LAST_LINE_START /* Anchor */,
																GridBagConstraints.HORIZONTAL /* fill */,
																new Insets(0,5,0,0),
																0 /* ipadx */,
																0 /* ipady*/ ));
		
		strokeOrder = new JTextField(10);
		main.add(strokeOrder, new GridBagConstraints(1 /* gridx */, 
													1 /* gridy */, 
													1 /* gridweight */, 
													1 /* gridheight */, 
													1.0 /* weightx */,
													1.0 /* weighty */,
													GridBagConstraints.LAST_LINE_START /* Anchor */,
													GridBagConstraints.HORIZONTAL /* fill */,
													new Insets(0,0,0,5),
													0 /* ipadx */,
													0 /* ipady*/ ));
		main.add(new JLabel("Stroke Order:"), new GridBagConstraints(	0 /* gridx */, 
																1 /* gridy */, 
																1 /* gridweight */, 
																1 /* gridheight */, 
																1.0 /* weightx */,
																1.0 /* weighty */,
																GridBagConstraints.LAST_LINE_START /* Anchor */,
																GridBagConstraints.HORIZONTAL /* fill */,
																new Insets(0,5,0,0),
																0 /* ipadx */,
																0 /* ipady*/ ));
		
		govFreq = new JTextField(10);
		main.add(govFreq, new GridBagConstraints(1 /* gridx */, 
												2 /* gridy */, 
												1 /* gridweight */, 
												1 /* gridheight */, 
												1.0 /* weightx */,
												1.0 /* weighty */,
												GridBagConstraints.LAST_LINE_START /* Anchor */,
												GridBagConstraints.HORIZONTAL /* fill */,
												new Insets(0,0,0,5),
												0 /* ipadx */,
												0 /* ipady*/ ));
		main.add(new JLabel("Gov. Freq.:"), new GridBagConstraints(	0 /* gridx */, 
																2 /* gridy */, 
																1 /* gridweight */, 
																1 /* gridheight */, 
																1.0 /* weightx */,
																1.0 /* weighty */,
																GridBagConstraints.LAST_LINE_START /* Anchor */,
																GridBagConstraints.HORIZONTAL /* fill */,
																new Insets(0,5,0,0),
																0 /* ipadx */,
																0 /* ipady*/ ));
										
		JPanel controls = new JPanel();
		controls.setLayout(new FlowLayout());
		controls.setOpaque(false);
		add = new JButton("Add");
		quit = new JButton("Quit");
		controls.add(add);
		controls.add(quit);
		

		main.add(controls, new GridBagConstraints(	0 /* gridx */, 
													3 /* gridy */, 
													2 /* gridweight */, 
													1 /* gridheight */, 
													1.0 /* weightx */,
													1.0 /* weighty */,
													GridBagConstraints.LAST_LINE_START /* Anchor */,
													GridBagConstraints.HORIZONTAL /* fill */,
													new Insets(0,0,0,0),
													0 /* ipadx */,
													0 /* ipady*/ ));
		
		return main;
	}
	/*
     *  Usage: Toggle the main frame visibility status from on to off.
     */
    public void toggle(){
        if(this.frame.isVisible())
            this.frame.setVisible(false);
        else
            this.frame.setVisible(true);
    }
	private void setLook(){
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            ex.printStackTrace();
        }
    }
}
