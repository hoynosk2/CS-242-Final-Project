package tools.mysql.entry.audiogatherer;

import java.awt.AlphaComposite;
import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.RenderingHints;

import javax.swing.JLabel;
import javax.swing.JPanel;

import linguistics.KoreanSymbol;

public class CharacterDisplayPane extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID 				= 1L;

	public final static String 	UNINITIALIZED_BLOCK 			= "欲";
	public final static String 	NO_NEXT_SYMBOL 				= "✘";
	public final static int 		INVALID_COUNT_DOWN			= -1;
	
	private final static String 	FONT 						= "Arial";
	private final static int 	CURRENT_CHARACTER_FONT_SIZE 	= 150;
	private final static int 	NEXT_CHARACTER_FONT_SIZE 	= 15;
	private final static int 	COUNT_DOWN_FONT_SIZE 		= 200;
	private final static float 	COUNT_DOWN_ALPHA				= 0.7f;
	
	private final static int	    PANEL_PIXEL_INC 				= 5;
	
	private String currentCharacter = UNINITIALIZED_BLOCK, nextCharacter = UNINITIALIZED_BLOCK;
	private JLabel currentCharacterLabel = null, nextCharacterLabel = null;
	private int countDown = INVALID_COUNT_DOWN;
	public CharacterDisplayPane() {
		_initialize();
	}
	public void change(KoreanSymbol currentCharacter, KoreanSymbol nextCharacter) {
		if(currentCharacter == null)
			return;
		this.currentCharacter = currentCharacter.getSymbol();
		
		if(nextCharacter != null)
			this.nextCharacter = nextCharacter.getSymbol();
		else
			this.nextCharacter = NO_NEXT_SYMBOL;
		currentCharacterLabel.setText(this.currentCharacter);
		nextCharacterLabel.setText(this.nextCharacter);
	}
	public void changeCountDown(int countDown) {
		this.countDown = countDown;
	}
	public void resetCountDown() {
		this.countDown = INVALID_COUNT_DOWN;
	}
	public String getCurrentCharacter() {
		return currentCharacter;
	}
	public String getNextCharacter() {
		return nextCharacter;
	}
	
	private void _initialize() {
		this.setLayout(new GridBagLayout());
		this.setOpaque(true);
		this.setBackground(Color.WHITE);
		_setComponents();
	}
	
	private void _setComponents() {
		/* Current Character Display Configuration */
		JPanel currentCharacterPane = new JPanel();
		currentCharacterPane.setOpaque(false);
		currentCharacterLabel = new JLabel(currentCharacter);
		currentCharacterLabel.setFont(new Font(FONT, Font.BOLD,  CURRENT_CHARACTER_FONT_SIZE));
		currentCharacterPane.add(currentCharacterLabel, BorderLayout.CENTER);
		this.add(currentCharacterPane, new GridBagConstraints(	0 /* gridx */, 
																0 /* gridy */, 
																1 /* gridweight */, 
																2 /* gridheight */, 
																0.95 /* weightx */,
																0.95 /* weighty */,
																GridBagConstraints.FIRST_LINE_START /* Anchor */,
																0 /* fill */,
																new Insets(0,0,0,0),
																0 /* ipadx */,
																0 /* ipady*/ ));
		
		/* Next Character Display Configuration */
		JPanel nextCharacterPane = new JPanel() {
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
		    protected void paintComponent(Graphics g) {
		        super.paintComponent(g);
		        Graphics2D g2 = (Graphics2D) g;
                g2.setStroke(new BasicStroke(1));
		        g2.setColor(Color.GRAY);  
		        
		        int radius = -1;
		        int x = -1;
		        int y = -1;
		        if(getSize().width >= getSize().height) {
		        		radius = (int)((getSize().height - 1 - (PANEL_PIXEL_INC / 2.0)) / 2.0);
		        		x = (int)((getSize().width - 1 - PANEL_PIXEL_INC) / 2.0 - radius);
		        		y = (PANEL_PIXEL_INC / 2);
		        } else {
		        		radius = (int)((getSize().width - 1 - (PANEL_PIXEL_INC / 2.0)) / 2.0);
		        		x = (PANEL_PIXEL_INC / 2);
		        		y = (int)((getSize().height - 1 - PANEL_PIXEL_INC) / 2.0 - radius);
		        }
		        
		        g2.drawOval(x, y, radius * 2, radius * 2);
		    }
		};
		nextCharacterPane.setOpaque(false);
		nextCharacterLabel = new JLabel(nextCharacter);
		nextCharacterLabel.setForeground(Color.GRAY);
		nextCharacterLabel.setFont(new Font(FONT, Font.PLAIN,  NEXT_CHARACTER_FONT_SIZE));
		nextCharacterPane.add(nextCharacterLabel, BorderLayout.CENTER);
		this.add(nextCharacterPane, new GridBagConstraints(		1 /* gridx */, 
																1 /* gridy */, 
																1 /* gridweight */, 
																1 /* gridheight */, 
																0.05 /* weightx */,
																0.05 /* weighty */,
																GridBagConstraints.LAST_LINE_END /* Anchor */,
																0 /* fill */,
																new Insets(0,0,0,5),
																PANEL_PIXEL_INC /* ipadx */,
																PANEL_PIXEL_INC /* ipady*/ ));
	}
	
	// Note: 	https://stackoverflow.com/questions/18565066/centering-string-in-panel
	// Note: 	do not override paintComponent, but rather paint(), because countDown number
	// 			should be printed onto of child components and paint() is called after loading children components
	// 			verse paintComponent which is not.
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		
		if(countDown != INVALID_COUNT_DOWN) {
			Graphics2D g2d = (Graphics2D) g.create();
			g2d.setColor(Color.RED);
			g2d.setFont(new Font(FONT, Font.BOLD, COUNT_DOWN_FONT_SIZE));
			g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
	        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, COUNT_DOWN_ALPHA));
			
	        FontMetrics fm = g2d.getFontMetrics();
	        int x = (getWidth() - fm.stringWidth(Integer.toString(countDown))) / 2;
	        int y = ((getHeight() - fm.getHeight()) / 2) + fm.getAscent();
	        
	        g2d.drawString(Integer.toString(countDown), x, y);
	        g2d.dispose();
		}
	}
}
