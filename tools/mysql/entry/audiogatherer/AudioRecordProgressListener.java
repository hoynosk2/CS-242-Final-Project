package tools.mysql.entry.audiogatherer;

public interface AudioRecordProgressListener {
	public void progressFinished();
}
