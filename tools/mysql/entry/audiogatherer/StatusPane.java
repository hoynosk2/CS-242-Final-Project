package tools.mysql.entry.audiogatherer;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.time.LocalDateTime;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;

public class StatusPane extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel status;
	public StatusPane() {
		_initialize();
	}
	private void _initialize() {
		this.setLayout(new GridBagLayout());
		this.setOpaque(true);
		this.setBackground(Color.WHITE);
		_setComponents();
	}
	protected void updateStatus(String status) {
		this.status.setText(status);
	}
	private void _setComponents() {
		JPanel statusPanel = new JPanel();
		statusPanel.setLayout(new BoxLayout(statusPanel, BoxLayout.X_AXIS));
		statusPanel.setBorder(BorderFactory.createLineBorder(new Color(0xBEBEBE)));
		statusPanel.setBackground(new Color(0xEEEEEE));
		JLabel statusLabel = new JLabel("Status: ");
		statusLabel.setHorizontalAlignment(SwingConstants.LEFT);
		status = new JLabel();
		statusPanel.add(statusLabel);
		statusPanel.add(status);
		
		this.add(statusPanel, new GridBagConstraints(0 /* gridx */, 
													0 /* gridy */, 
													1 /* gridweight */, 
													1 /* gridheight */, 
													1.0 /* weightx */,
													1.0 /* weighty */,
													GridBagConstraints.FIRST_LINE_START /* Anchor */,
													GridBagConstraints.HORIZONTAL /* fill */,
													new Insets(0, 0, 0, 0),
													0 /* ipadx */,
													0 /* ipady*/ ));
	}
	public static class Status extends StatusPane {
		private StatusPane statusPane;
		public Status(StatusPane statusPane) {
			this.statusPane = statusPane;
		}
		public void changeStatus(String text) {
			this.statusPane.updateStatus(text);
		}
	}
}
