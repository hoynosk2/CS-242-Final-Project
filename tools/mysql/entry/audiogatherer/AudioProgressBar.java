package tools.mysql.entry.audiogatherer;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Composite;
import java.awt.FontMetrics;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import javax.swing.JProgressBar;
import java.awt.Font;
import java.awt.RenderingHints;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import javax.swing.SwingWorker;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
/**
 * @description A custom Progress Bar that runs specifically on three sections of times.
 * @references  https://stackoverflow.com/questions/14036173/how-can-you-make-a-progress-bar-without-using-jprogressbar
 *              https://stackoverflow.com/questions/2593446/progress-bar-increment-by-1-every-100th-of-second
 */
public class AudioProgressBar extends JProgressBar{
    private static final long serialVersionUID = 1L;

    private static final Color gradientEndingColor = new Color(0xc0c0c0); // Silver
    private static final Color borderColor = new Color(0x736a60); 		// Sandstone in color
    private static final Color disabledBorderColor = new Color(0xbebebe); // Silver
    private static final Color AUDIO_PLAYING_COLOR = new Color(0x6FA8DC); // Jordy Blue
    
    // Details all the needed levels of transparancy needed during painting of the progress bar.
    private static final Composite gradientTransparancy = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.25f);
    private static final Composite OPAQUE = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1.0f);
    private static final Composite BACKGROUND_TRANSPARENCY = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.7f);
    private static final Composite PROGRESS_TRANSPARENCY = AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.3f);
    
    private static final String QUIET_AREA_STR = "Quiet";
    private static final String SPEAK_AREA_STR = "Speak";
    private static final Font AREA_FONT = new Font("Arial", Font.BOLD,  15);
    
    private static GradientPaint gradient; // Used for a filter over the progress bar; acts as a finish.
    
    private int oldWidth;   // Used to valident on whether or not the gradient should be rest.
    private int oldHeight;  // Used to valident on whether or not the gradient should be rest.

    private int displayWidth;
    private int displayHeight;
    
    // Pushed poition boundaries for the progress bar.
    private int insets[] = new int[4];
    private static final int TOP_INSET = 0;
    private static final int LEFT_INSET = 1;
    private static final int BOTTOM_INSET = 2;
    private static final int RIGHT_INSET = 3;
    
    public static final int RECORD_MODE = 0; 	// Details a three time area progress bar (two red and one green)
    public static final int PLAY_MODE = 1;		// Used for just playing audio back; represents a single aread of time (in blue).
    public static final Color DEFAULT_PROGRESS_COLOR = Color.BLUE;
    
    // First an action when the progress bar has finished.
    private List<AudioRecordProgressListener> listeners = new ArrayList<AudioRecordProgressListener>();
    
    private Color progressColor;
    private Timer timer; 					// Used to increment the progress bar
    private int quietSeconds, speakSeconds; 	// The RECORD_MODE time details.
    private int progress = 0; 				// Current Progress.
    private int maxTicks;				 	// The total number of ticks on the progress bar.
    private int currentMode = RECORD_MODE; 	// Current mode of the progress bar.
    private int slice = 10; 					// Number of milliseconds between each tick on the progress bar.
    
    /**
     * @description Creates a progress bar in record mode!
     * @param quietSeconds Number of quiet seconds on either end of the bar.
     * @param speakSeconds The middle time second duration, used for speaking.
     */
    public AudioProgressBar(int quietSeconds, int speakSeconds){
        progressColor = DEFAULT_PROGRESS_COLOR;
        
        this.quietSeconds = quietSeconds;
        this.speakSeconds = speakSeconds;
        
        changeMode(RECORD_MODE, 2 * quietSeconds + speakSeconds); // Changes the mode and configures the time for that mode.
    }
    
    /**
     * @description Gets the record mode time.
     * @return Record mode total time
     */
    public int getRecordTime() {
    		return 2 * quietSeconds + speakSeconds;
    }
    
    /**
     * @description Gets the current mode being used.
     * @return Mode of progress bar currently.
     */
    public int getMode() {
    		return currentMode;
    }
    
    /**
     * @description Computers number of total ticks need for a smooth progress bar given 'x' number of seconds.
     * @param seconds Progress Bar's progress lifespan.
     * @return Total ticks needs for the progress bar.
     */
    private int _getMaxProgressTicks(int seconds){
        return ((seconds) * 1000) / slice;
    }
    
    /**
     * @description Changes the mode of the progress bar effectively.
     * @param type The mode type switching too.
     * @param seconds The new duration of the progress bar.
     */
    public void changeMode(int type, int seconds) {
    		maxTicks = this._getMaxProgressTicks(seconds);
        super.setMaximum(maxTicks);
        super.setMinimum(0);
        super.setValue(0);
        progress = 0;
        currentMode = type;
    }
    
    /**
     * @description Paints the actual bar.
     * @param g The current graphics of the progress bar.
     */
    @Override
    protected void paintComponent(Graphics g){
        int w = displayWidth != 0 ? displayWidth - 1 : getWidth() - 1; 		// Pixel width.
        int h = displayHeight != 0 ? displayHeight - 1 : getHeight() - 1;		// Pixel height.
        
        /* Applies any insets to the progress bar. */
        int x = insets[LEFT_INSET];
        int y = insets[TOP_INSET];
        w -= (insets[RIGHT_INSET] << 1);
        h -= (insets[BOTTOM_INSET] << 1);
        
        int min = getMinimum();
        int max = getMaximum();
        int total = max - min;
        float dx = (float) (w - 2) / (float) total; // Gets pixel with of a single progress bar tick.
        int value = getValue(); // Current progress value of the bar.
        
        // Sets the gradient that will be placed ontop of progress bar.
        if (gradient == null) {
            gradient = new GradientPaint(0.0f, 0.0f, Color.WHITE, 0.0f, h, gradientEndingColor);
        }
        
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON); // Allows Transparency
        g2d.setFont(AREA_FONT); // Fonts main font of the progress bar sections.
        
        // Clean the background
        if (isOpaque()) {
            g2d.setColor(getBackground());
            g2d.fillRect(0, 0, getWidth(), getHeight());
        }
        
        //Background Colors
        g2d.setComposite(BACKGROUND_TRANSPARENCY);
        if(currentMode == RECORD_MODE){
            int quietStrPixelLength = g2d.getFontMetrics().stringWidth(QUIET_AREA_STR); 	// Length in pixels of the 'quiet' string.
            int speakStrPixelLength = g2d.getFontMetrics().stringWidth(SPEAK_AREA_STR); 	// Length in pixels of the 'speak' string.
            int strPixelHeight = g2d.getFontMetrics(AREA_FONT).getHeight(); 				// Gets the pixel height of the font.
            int quietValue = (quietSeconds * 1000) / slice; // Number of ticks on the progressbar for a quiet area pass through.
            int speakValue = (speakSeconds * 1000) / slice; // Number of ticks on the progressbar for the speaking area.
            
            double quietAreaPixelWidth = dx * quietValue;
            double speakAreaPixelWidth = dx * speakValue;
            double startingPixelWidthLoc = 1;
            
            int areaTextEndYCord = (int)Math.floor((h / 2.0) + (strPixelHeight / 2.0)) - 3; // "-3" was chosen as it appears better on mac.
            
            /* Initial Quiet Area */
            g2d.setColor(Color.RED);
            g2d.fillRect((int)startingPixelWidthLoc, 1, (int) quietAreaPixelWidth, h - 1);
            startingPixelWidthLoc += quietAreaPixelWidth;
            g2d.setColor(Color.BLACK);
            g2d.drawString("Quiet", (int)(quietAreaPixelWidth / 2) - (quietStrPixelLength / 2), areaTextEndYCord);
            
            /* Speaking Area */
            g2d.setColor(Color.GREEN);
            g2d.fillRect((int) startingPixelWidthLoc, 1, (int) speakAreaPixelWidth + 1, h - 1);
            g2d.setColor(Color.BLACK);
            g2d.drawString("Speak",(int) (startingPixelWidthLoc + (speakAreaPixelWidth / 2) - (speakStrPixelLength / 2)), areaTextEndYCord);
            startingPixelWidthLoc += speakAreaPixelWidth;
            
            /* Ending Quiet Area */
            g2d.setColor(Color.RED);
            g2d.fillRect((int) startingPixelWidthLoc, 1, (int) quietAreaPixelWidth + 1, h - 1);
            g2d.setColor(Color.BLACK);
            g2d.drawString("Quiet", (int) (startingPixelWidthLoc + (quietAreaPixelWidth / 2) - (quietStrPixelLength / 2)) + 1, areaTextEndYCord);
        }else {
        		// Changes the bar to just be pure blue during audio play back mode.
            g2d.setColor(AUDIO_PLAYING_COLOR);
            g2d.fillRect(0, 0, getWidth(), getHeight());
        }
        g2d.setComposite(OPAQUE);
        g2d.translate(x, y); // Pushes progessbar to new x and y position (Insets might have moved it).
        
        // Draws the control border around the progress bar.
        g2d.setColor(isEnabled() ? borderColor : disabledBorderColor);
        g2d.drawLine(1, 0, w - 1, 0);
        g2d.drawLine(1, h, w - 1, h);
        g2d.drawLine(0, 1, 0, h - 1);
        g2d.drawLine(w, 1, w, h - 1);
        
        // Fill in the progress accordingly
        int progress = 0; 
        if (value == max) {
            progress = w - 1;
        } else {
            progress = (int) (dx * getValue()); // How many pixels to fill in in the width direction.        
        }
	    
        // Fills the actual progress in.
	    g2d.setComposite(PROGRESS_TRANSPARENCY);
        g2d.setColor(progressColor);
        g2d.fillRect(1, 1, progress, h - 1);
        
        // A gradient over the progress fill
        g2d.setPaint(gradient);
        g2d.setComposite(gradientTransparancy);
        g2d.fillRect(1, 1, w - 1, (h >> 1)); // Upper rectangle.
        final float FACTOR = 0.20f;
        g2d.fillRect(1, h - (int) (h * FACTOR), w - 1, (int) (h * FACTOR)); // Bottom rectangle.

    }
    
    /**
     * @description Used to increment the progress bar in a very smooth motion to match the correct time table.
     */
    public void start(){
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            int time = 0;
            @Override
            public void run() {
                progress++;
                setValue(progress);
                time += slice;
                if(progress >= maxTicks){
                		// Fires any events if available.
                		for (AudioRecordProgressListener hl : listeners)
                			hl.progressFinished();
                		this.cancel();
                }
            }
        }, slice, slice);
    }
    
    /**
     * @description Stops the progress bar pre-maturely 
     */
    public void stop(){
        progress = 0;
        setValue(0);
        timer.cancel();
    }
    
    /**
     * @description Adds a progress listener to the current bar.
     * @param event The event to add.
     */
    public void addProgressListener(AudioRecordProgressListener event) {
    		listeners.add(event);
    }
    
    /**
     * @description Used to re-validate the size of the bar in cases of repainting.
     */
    @Override
    public void validate() {
        int w = getWidth();
        int h = getHeight();
        super.validate();
        if (oldWidth != w || oldHeight != h) {
            oldWidth = w;
            oldHeight = h;
            gradient = null; // Rest gradient as component width and/or height has changed.
        }
    }
    
    /**
     * @description Don't paint a border as this is done above.
     */
    @Override
    protected void paintBorder(Graphics g) {
        /* Do not allow border as this is taken care of by paintComponent. */
    }
    
    /**
     * @description Don't allowed setting of maximum, as max ticks are configured above.
     */
    @Override
    public void setMaximum(int n) {
        /* Do not allow max to be set as this is done in the constructor. */
    }
}
