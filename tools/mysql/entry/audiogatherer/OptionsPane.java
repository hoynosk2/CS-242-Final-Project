package tools.mysql.entry.audiogatherer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;
import javax.swing.text.NumberFormatter;

import tools.Console;
import tools.mysql.DatabaseAccess;
import tools.mysql.entry.audiogatherer.StatusPane.Status;

public class OptionsPane extends JPanel {
	
	/**
	 * 
	 */
	public static final int AUTO_REPLAY_AMOUNT = 1;
	
	private static final long serialVersionUID = 1L;
	private static final String QUIET_NOISE_BUFFER_LABEL_STR = "Quiet Noise Additional Buffer";
	private final static String 	FONT 						= "Arial";
	private static final int 	OPTIONS_LABEL_FONT_SIZE		= 14;
	
	private static final String OPTIONS_TITLE = "Options";
	
	private static final String QUIEST_NOISE_HELP_BTN_TITLE 	= "Quiet Noise Increment Search";
	private static final String QUIEST_NOISE_HELP_BTN_MSG	 	= "Hello";
	private static final String QUIET_NOISE_OPTION_DEFAULT_VALUE = "000";
	private static final int 	QUIET_NOISE_OPTION_SLIDER_MIN = 0;
	private static final int 	QUIET_NOISE_OPTION_SLIDER_MAX = 999;
	
	private static final String 	NEXT_CHARACTER_LABEL_STR 	= "Next Character";
	private static final String 	STORE_IN_DATABASE_LABEL_STR 	= "Store in Database";
	private static final String 	AUTO_REPLAY_LABEL_STR		= "Auto-Replay Audio";
	
	private static final String QUIET_NOISE_STATUS_CHANGE		= "Quiet noise finder buffer changed to";
	
	private JButton quietNoiseHelpButton, nextCharacterHelpButton, storeInDatabaseHelpButton, autoReplayHelpButton;
	private JTextField quietNoiseOptionTxt, nextCharacterTxt;
	private JSlider quietNoiseOptionSlider;
	private JCheckBox storeInDatabaseCB, autoReplayCB;
	private Status status;
	private CharacterDisplayPane characterDisplay;
	public OptionsPane(Status status, CharacterDisplayPane characterDisplay) {
		this.status = status;
		this.characterDisplay = characterDisplay;
		_initialize();
	}
	
	private void _initialize() {
		this.setLayout(new GridBagLayout());
		this.setOpaque(true);
		this.setBackground(Color.WHITE);
		_setComponents();
		_setActions();
	}
	public int getQuietNoiseBufferValue() {
		return Integer.parseInt(quietNoiseOptionTxt.getText());
	}
	public boolean isAutoReplayChecked() {
		return autoReplayCB.isSelected();
	}
	private void _setComponents() {
		//JLabel title = new JLabel(OPTIONS_TITLE);
		//title.setFont(new Font(FONT, Font.BOLD,  OPTIONS_LABEL_FONT_SIZE));
		//this.add(title, new GridBagConstraints(	0 /* gridx */, 
		//		0 /* gridy */, 
		//		1 /* gridweight */, 
		//		1 /* gridheight */, 
		//		1.0 /* weightx */,
		//		1.0 /* weighty */,
		//		GridBagConstraints.CENTER /* Anchor */,
		//		0 /* fill */,
		//		new Insets(0,0,0,0),
		//		0 /* ipadx */,
		//		0 /* ipady*/ ));
		
		/* Quiet Noise Addition Buffer Option */
		JPanel quietNoiseOptionPane = new JPanel();
		quietNoiseOptionPane.setLayout(new FlowLayout());
		quietNoiseOptionPane.setOpaque(false);
		JLabel quietNoiseBufferLabel = new JLabel(QUIET_NOISE_BUFFER_LABEL_STR);
		quietNoiseBufferLabel.setFont(new Font(FONT, Font.PLAIN,  OPTIONS_LABEL_FONT_SIZE));
		quietNoiseHelpButton = new JButton();
		quietNoiseHelpButton.putClientProperty("JButton.buttonType", "help");
		
		
		quietNoiseOptionPane.add(quietNoiseBufferLabel);
		quietNoiseOptionPane.add(quietNoiseHelpButton);
		
		this.add(quietNoiseOptionPane, new GridBagConstraints(	0 /* gridx */, 
																1 /* gridy */, 
																1 /* gridweight */, 
																1 /* gridheight */, 
																1.0 /* weightx */,
																1.0 /* weighty */,
																GridBagConstraints.LINE_START /* Anchor */,
																0 /* fill */,
																new Insets(0,0,-5,0),
																0 /* ipadx */,
																0 /* ipady*/ ));
		
		JPanel quietNoiseOptionControlPane = new JPanel();
		quietNoiseOptionControlPane.setLayout(new FlowLayout());
		quietNoiseOptionControlPane.setOpaque(false);
		
		quietNoiseOptionTxt = new JTextField(QUIET_NOISE_OPTION_DEFAULT_VALUE, String.valueOf(QUIET_NOISE_OPTION_SLIDER_MAX).length() - 1);
		//https://stackoverflow.com/questions/24844559/jtextfield-using-document-filter-to-filter-integers-and-periods
		//https://docs.oracle.com/javase/7/docs/api/java/util/regex/Pattern.html
		AbstractDocument document = (AbstractDocument) quietNoiseOptionTxt.getDocument();
		document.setDocumentFilter(new DocumentFilter() {
            public void replace(FilterBypass fb, int offs, int length, String str, AttributeSet a) throws BadLocationException {
                String text = fb.getDocument().getText(0, fb.getDocument().getLength());
                text += str;
                if ((fb.getDocument().getLength() + str.length() - length) <= String.valueOf(QUIET_NOISE_OPTION_SLIDER_MAX).length()
                        && text.matches("^[0-9]*{0,3}$")) {
                    super.replace(fb, offs, length, str, a);
                } else {
                    Toolkit.getDefaultToolkit().beep();
                }
            }

            public void insertString(FilterBypass fb, int offs, String str, AttributeSet a) throws BadLocationException {
                String text = fb.getDocument().getText(0, fb.getDocument().getLength());
                text += str;
                if ((fb.getDocument().getLength() + str.length()) <= String.valueOf(QUIET_NOISE_OPTION_SLIDER_MAX).length()
                        && text.matches("^[0-9]*{0,3}$")) {
                    super.insertString(fb, offs, str, a);
                } else {
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
		
		quietNoiseOptionSlider = new JSlider(JSlider.HORIZONTAL, QUIET_NOISE_OPTION_SLIDER_MIN, QUIET_NOISE_OPTION_SLIDER_MAX, 0);
		quietNoiseOptionSlider.setMajorTickSpacing(249);
		quietNoiseOptionSlider.setMinorTickSpacing(50);
		quietNoiseOptionSlider.setPaintTicks(false);
		quietNoiseOptionSlider.setPaintLabels(false);
		
		quietNoiseOptionControlPane.add(quietNoiseOptionTxt);
		quietNoiseOptionControlPane.add(quietNoiseOptionSlider);
		
		this.add(quietNoiseOptionControlPane, new GridBagConstraints(	0 /* gridx */, 
																	2 /* gridy */, 
																	1 /* gridweight */, 
																	1 /* gridheight */, 
																	1.0 /* weightx */,
																	1.0 /* weighty */,
																	GridBagConstraints.LINE_START /* Anchor */,
																	GridBagConstraints.HORIZONTAL /* fill */,
																	new Insets(-5,0,0,0),
																	0 /* ipadx */,
																	0 /* ipady*/ ));
		
		JPanel nextCharacterOptionPane = new JPanel();
		nextCharacterOptionPane.setLayout(new FlowLayout());
		nextCharacterOptionPane.setOpaque(false);
		JLabel nextCharacterLabel = new JLabel(NEXT_CHARACTER_LABEL_STR);
		nextCharacterHelpButton = new JButton();
		nextCharacterHelpButton.putClientProperty("JButton.buttonType", "help");
		nextCharacterTxt = new JTextField(CharacterDisplayPane.UNINITIALIZED_BLOCK, 1);
		AbstractDocument nextCharacterTxtDocument = (AbstractDocument) nextCharacterTxt.getDocument();
		nextCharacterTxtDocument.setDocumentFilter(new DocumentFilter() {
            public void replace(FilterBypass fb, int offs, int length, String str, AttributeSet a) throws BadLocationException {
                if ((fb.getDocument().getLength() + str.length() - length) <= 1) {
                    super.replace(fb, offs, length, str, a);
                } else {
                    Toolkit.getDefaultToolkit().beep();
                }
            }

            public void insertString(FilterBypass fb, int offs, String str, AttributeSet a) throws BadLocationException {
                if ((fb.getDocument().getLength() + str.length()) <= 1) {
                    super.insertString(fb, offs, str, a);
                } else {
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        });
		
		nextCharacterOptionPane.add(nextCharacterTxt);
		nextCharacterOptionPane.add(nextCharacterLabel);
		nextCharacterOptionPane.add(nextCharacterHelpButton);
		
		this.add(nextCharacterOptionPane, new GridBagConstraints(	0 /* gridx */, 
																				3 /* gridy */, 
																				1 /* gridweight */, 
																				1 /* gridheight */, 
																				1.0 /* weightx */,
																				1.0 /* weighty */,
																				GridBagConstraints.LINE_START /* Anchor */,
																				GridBagConstraints.NONE /* fill */,
																				new Insets(0,0,0,0),
																				0 /* ipadx */,
																				0 /* ipady*/ ));
		
		storeInDatabaseHelpButton = new JButton();
		storeInDatabaseCB = new JCheckBox();
		this.add(_labeledCheckBox(storeInDatabaseHelpButton, STORE_IN_DATABASE_LABEL_STR, storeInDatabaseCB), new GridBagConstraints(	0 /* gridx */, 
																																	4 /* gridy */, 
																																	1 /* gridweight */, 
																																	1 /* gridheight */, 
																																	1.0 /* weightx */,
																																	1.0 /* weighty */,
																																	GridBagConstraints.LINE_START /* Anchor */,
																																	0 /* fill */,
																																	new Insets(0,0,-5,0),
																																	0 /* ipadx */,
																																	0 /* ipady*/ ));
		
		autoReplayHelpButton = new JButton();
		autoReplayCB = new JCheckBox();
		this.add(_labeledCheckBox(autoReplayHelpButton, AUTO_REPLAY_LABEL_STR, autoReplayCB), new GridBagConstraints(	0 /* gridx */, 
																													5 /* gridy */, 
																													1 /* gridweight */, 
																													1 /* gridheight */, 
																													1.0 /* weightx */,
																													1.0 /* weighty */,
																													GridBagConstraints.LINE_START /* Anchor */,
																													0 /* fill */,
																													new Insets(0,0,-5,0),
																													0 /* ipadx */,
																													0 /* ipady*/ ));
		
		
	}
	
	private JPanel _labeledCheckBox(JButton helpButton, String labelStr, JCheckBox checkBox) {
		JPanel pane = new JPanel();
		pane.setLayout(new FlowLayout());
		pane.setOpaque(false);
		
		JLabel label = new JLabel(labelStr);
		helpButton.putClientProperty("JButton.buttonType", "help");
		
		checkBox.setSelected(false);
		
		pane.add(checkBox);
		pane.add(label);
		pane.add(helpButton);
		
		return pane;
	}
	
	private void _setActions() {
		quietNoiseHelpButton.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		        Console.simpleOutput(_getTopFrame(), QUIEST_NOISE_HELP_BTN_TITLE, QUIEST_NOISE_HELP_BTN_MSG, false);
		    }
		});
		
		quietNoiseOptionSlider.addChangeListener(new ChangeListener() {
	        @Override
	        public void stateChanged(ChangeEvent ce) {
	        		int currentValue = ((JSlider) ce.getSource()).getValue();
	        		final NumberFormat numFormat = NumberFormat.getInstance();
	        		numFormat.setMinimumIntegerDigits(0);
	        		numFormat.setMaximumIntegerDigits(3);
	        		quietNoiseOptionTxt.setText(numFormat.format(currentValue));
	        		status.changeStatus(QUIET_NOISE_STATUS_CHANGE + " " + numFormat.format(currentValue));
	        }
		});
		
		quietNoiseOptionTxt.getDocument().addDocumentListener(new DocumentListener() {
			
			final Runnable changeSlider = new Runnable() {
                @Override
                public void run() {
                		if(!quietNoiseOptionTxt.getText().equals(""))
                			quietNoiseOptionSlider.setValue(Integer.parseInt(quietNoiseOptionTxt.getText()));
                }
            };
			@Override
			public void insertUpdate(DocumentEvent e) {
                SwingUtilities.invokeLater(changeSlider);
			}

			@Override
			public void removeUpdate(DocumentEvent e) {
                SwingUtilities.invokeLater(changeSlider);
			}

			@Override
			public void changedUpdate(DocumentEvent e) {
				/* Nothing to see here. */
			}
			
		});
		
		storeInDatabaseHelpButton.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		        Console.simpleOutput(_getTopFrame(), QUIEST_NOISE_HELP_BTN_TITLE, QUIEST_NOISE_HELP_BTN_MSG, false);
		    }
		});
		
	}
	
	private JFrame _getTopFrame() {
		return (JFrame) SwingUtilities.getWindowAncestor(this);
	}
}
