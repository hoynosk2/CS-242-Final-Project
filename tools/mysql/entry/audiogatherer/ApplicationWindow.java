package tools.mysql.entry.audiogatherer;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.sql.SQLException;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import identification.Version;
import linguistics.KoreanSymbol;
import tools.mysql.DatabaseAccess;
import tools.mysql.entry.audiogatherer.StatusPane.Status;

/**
 * @description Represents the audio studio tools front end application window.
 * @author dan
 *
 */
public class ApplicationWindow {
	
	private static final String STEP_BTN_STR = "Save/Step";
	private static final String SAVE_BTN_STR = "Save";
	private static final String CYCLE_BTN_STR = "Start Cycle";
	private static final String QUIT_BTN_STR = "Quit";
	
	/* Status Strings */
	private static final String DATABASE_CONNECTION_FAILED = "Database failed to connect.";
	
	private JFrame frame; 							// Main frame to display everything in
	private JButton step, cycle, quit;				// Main control buttons for navigating between characters.
	private DatabaseAccess dba;						// Gives access to the database.
	private Status status;							// A status bar allows for message updating for the user.
	private KoreanSymbol[] symbols;					// The current and next symbols in line.
	private CharacterDisplayPane characterDisplay;	// The display that displays the current and next symbls.
	public ApplicationWindow(final String title) {
		this.frame = new JFrame(title + " " + Version.VERSION_STR);
		this.frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		this.frame.setResizable(false);
		this.setLook(); 																	// Sets the look to be default to the operating system.
		dba = new DatabaseAccess(DatabaseAccess.MASTER_USER); 							// Use Master User as read and write access is needed.
		this.frame.getContentPane().add(this._getMainContentPane(), BorderLayout.CENTER); // Places main content pain in the center of the frame for best viewing. 
		_setActions();
		_initialize();
		this.frame.pack(); // Must be called to reorganize and compact the frame together.
	}
	
	/**
	 * @description Opens the database connection and configures the character display for the first two characters.
	 */
	private void _initialize() {
		try {
			dba.open();
			symbols = dba.getNextFrequentSymbolsWithoutAudio();	// Gets the next two frequent symbols
			characterDisplay.change(symbols[0], symbols[1]);
		} catch (SQLException e) {
			status.changeStatus(DATABASE_CONNECTION_FAILED);
		}
	}
	
	/**
	 * @description Sets all the actions that correspond to buttons, etc.
	 */
	private void _setActions() {
		quit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
			}
		});
	}
	
	/**
	 * @description The main content pane that contains all the graphics elements to be displayed on screen. Everything is built in here.
	 * @return The main content pane, which has all the comments inside of.
	 */
	private JPanel _getMainContentPane() {
		JPanel main = new JPanel();
		main.setLayout(new GridBagLayout());
		main.setOpaque(true);				// If allowed to be transparent then background cannot be set.
		main.setBackground(Color.WHITE);
		
		// Status pane is used to display the status bar for delivering messages to the user.
		StatusPane statusPane = new StatusPane();
		main.add(statusPane, new GridBagConstraints(	0 /* gridx */, 
													3 /* gridy */, 
													2 /* gridweight */, 
													1 /* gridheight */, 
													1.0 /* weightx */,
													1.0 /* weighty */,
													GridBagConstraints.LAST_LINE_START /* Anchor */,
													GridBagConstraints.HORIZONTAL /* fill */,
													new Insets(0,0,0,0),
													0 /* ipadx */,
													0 /* ipady*/ ));
		
		status = new Status(statusPane); // A status wrapper, which is used to update the status bar.
		
		/* Character display configuration */
		TitledBorder characterDisplayTitle = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(0xBEBEBE)), "Character Block");
		characterDisplayTitle.setTitleJustification(TitledBorder.CENTER);	
		characterDisplay = new CharacterDisplayPane();
		characterDisplay.setOpaque(true); // Background is set by display, therefore must be opaque
		characterDisplay.setBorder(characterDisplayTitle);
		main.add(characterDisplay, new GridBagConstraints(	0 /* gridx */, 
															0 /* gridy */, 
															1 /* gridweight */, 
															1 /* gridheight */, 
															1.0 /* weightx */,
															1.0 /* weighty */,
															GridBagConstraints.FIRST_LINE_START /* Anchor */,
															GridBagConstraints.VERTICAL /* fill */,
															new Insets(5,5,0,0),
															0 /* ipadx */,
															0 /* ipady*/ ));
		
		/* Option panel configuration - Used to display all optional options used for either audio manipulation, database saving, and audio play back looping. */
		TitledBorder optionsTitle = BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(0xBEBEBE)), "Options");
		optionsTitle.setTitleJustification(TitledBorder.CENTER);	
		OptionsPane options = new OptionsPane(status, characterDisplay);
		options.setBorder(optionsTitle);
		options.setOpaque(true);
		main.add(options, new GridBagConstraints(	1 /* gridx */, 
													0 /* gridy */, 
													1 /* gridweight */, 
													1 /* gridheight */, 
													1.0 /* weightx */,
													1.0 /* weighty */,
													GridBagConstraints.FIRST_LINE_END /* Anchor */,
													GridBagConstraints.BOTH /* fill */,
													new Insets(5,0,0,5),
													0 /* ipadx */,
													0 /* ipady*/ ));
		
		/* The audio progress pane displays the audio time bar and the audio play/stop/record functionality buttons. */
		AudioProgressPane audioProgress = new AudioProgressPane(status, characterDisplay, options);
		audioProgress.setOpaque(true);
		main.add(audioProgress, new GridBagConstraints(	0 /* gridx */, 
														1 /* gridy */, 
														2 /* gridweight */, 
														1 /* gridheight */, 
														1.0 /* weightx */,
														1.0 /* weighty */,
														GridBagConstraints.FIRST_LINE_END /* Anchor */,
														GridBagConstraints.HORIZONTAL /* fill */,
														new Insets(5,7,0,7),
														0 /* ipadx */,
														0 /* ipady*/ ));
		
		/*
		 * Finally, the application window manages the main three buttons on screen. There are as such:
		 * Step: Steps over one symbol and heads to the next, if available. 
		 * Cycle: Cycles automatically through the symbols with no input needed by the user except through their voice.
		 * Quit: Quits the application promptly
		 */
		JPanel controls = new JPanel();
		controls.setLayout(new GridLayout(1,3));
		controls.setOpaque(false);
		step = new JButton(STEP_BTN_STR);
		step.setEnabled(false);
		step.putClientProperty("JButton.buttonType", "roundRect"); 	// Used to set the style of the button.
		step.putClientProperty("JComponent.sizeVariant", "small");	// Used to set the style size of the button.
		cycle = new JButton(CYCLE_BTN_STR);
		cycle.setEnabled(false);
		cycle.putClientProperty("JButton.buttonType", "roundRect");	// Used to set the style of the button.
		cycle.putClientProperty("JComponent.sizeVariant", "small");	// Used to set the style size of the button.
		quit = new JButton(QUIT_BTN_STR);
		quit.putClientProperty("JButton.buttonType", "roundRect");	// Used to set the style of the button.
		quit.putClientProperty("JComponent.sizeVariant", "small");	// Used to set the style size of the button.
		controls.add(step);
		controls.add(cycle);
		controls.add(quit);
		main.add(controls, new GridBagConstraints(	0 /* gridx */, 
													2 /* gridy */, 
													2 /* gridweight */, 
													1 /* gridheight */, 
													1.0 /* weightx */,
													1.0 /* weighty */,
													GridBagConstraints.CENTER /* Anchor */,
													GridBagConstraints.BOTH /* fill */,
													new Insets(0,0,0,0),
													0 /* ipadx */,
													0 /* ipady*/ ));
		
		
		
		return main;
	}
	
	/**
     *  @description Toggles the main frame visibility status from on to off.
     */
    public void toggle(){
        if(this.frame.isVisible())
            this.frame.setVisible(false);
        else
            this.frame.setVisible(true);
    }
    
    /**
     * @description Sets the application frame's look and feel to be that of the currently running operating system.
     */
	private void setLook(){
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            ex.printStackTrace();
        }
    }
}
