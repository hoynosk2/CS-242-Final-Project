package tools.mysql.entry.audiogatherer;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;

import audio.AudioCapture;
import audio.AudioData;
import audio.AudioManipulation;
import audio.AudioPlayer;
import audio.InvalidAudioChannelsException;
import tools.mysql.entry.audiogatherer.StatusPane.Status;

public class AudioProgressPane extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final String PLAY_BTN_STR 	= "▶";
	private static final String STOP_BTN_STR 	= "◼";
	private static final String RECORD_BTN_STR 	= "●";
	private static final String REFRESH_BTN_STR 	= "⟳";
	
	private static final String RECORD_START_STATUS = "The recording has started.";
	private static final String AUDIO_FAILED_TO_LOAD_STATUS = "The audio failed to load.";
	private static final String RECORD_PREMATURE_STOP_STATUS = "The recording has stopped prematurely. None was saved.";
	private static final String RECORD_FINISHED_STATUS = "The recording has successfully finished.";
	private static final String RECORD_FAIL_STATUS	= "The recording failed to start. Line may be unavalible.";
	private static final String AUDIO_PLAYER_FAIL_LOAD_STATUS = "The audio player failed to load. Line may be unavalible";
	private static final String AUDIO_FAILED_TO_PLAY = "The audio failed to play. Did the player fail to load?";
	
	
	
	private JButton playStop, record, refresh;
	private AudioProgressBar progressBar;
	private Status status;
	private boolean recording = false;
	private Timer prerecordCountdownTimer;
	private CharacterDisplayPane characterDisplay;
	private AudioCapture audioCapture;
	private AudioPlayer audioPlayer;
	private AudioData currentAudioData = AudioData.EMPTY_AUDIO;
	private OptionsPane options;
	public AudioProgressPane(Status status, CharacterDisplayPane characterDisplay, OptionsPane options) {
		this.status = status;
		this.characterDisplay = characterDisplay;
		this.options = options;
		prerecordCountdownTimer = new Timer();
		_initialize();
	}
	private void _initialize() {
		this.setLayout(new GridBagLayout());
		this.setOpaque(true);
		this.setBackground(Color.WHITE);
		_setComponents();
		_setActions();
		
		try {
			audioPlayer = new AudioPlayer();
		} catch (LineUnavailableException e) {
			status.changeStatus(AUDIO_PLAYER_FAIL_LOAD_STATUS);
			audioPlayer = null;
		}
	}
	private void _setComponents() {
		JPanel audioPane = new JPanel();
		audioPane.setLayout(new GridBagLayout());
		audioPane.setOpaque(false);
		
		JPanel audioControlPane = new JPanel();
		audioControlPane.setLayout(new GridBagLayout());
		audioControlPane.setOpaque(false);
		
		progressBar = new AudioProgressBar(AudioManipulation.SILENCE_SECTION_DURATION, AudioManipulation.SPEAKING_SECTION_DURATION);
		audioCapture = new AudioCapture((2 * AudioManipulation.SILENCE_SECTION_DURATION) + AudioManipulation.SPEAKING_SECTION_DURATION);
		
		playStop = new JButton(PLAY_BTN_STR);
 		playStop.putClientProperty("JButton.buttonType", "roundRect");
 		playStop.setMargin(new Insets(0,0,0,0));
 		playStop.setEnabled(false);
		//playStop.setPreferredSize(new Dimension(20, 20));
		record = new JButton(RECORD_BTN_STR);
		record.putClientProperty("JButton.buttonType", "roundRect");
		record.setForeground(Color.RED);
		record.setMargin(new Insets(0,0,0,0));
		//record.putClientProperty("JButton.buttonType", "help");
		refresh = new JButton(REFRESH_BTN_STR);
		refresh.putClientProperty("JButton.buttonType", "roundRect");
		refresh.setMargin(new Insets(0,0,0,0));
		refresh.setEnabled(false);
		//refresh.putClientProperty("JButton.buttonType", "help");
		audioControlPane.add(playStop, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.FIRST_LINE_START, 0, new Insets(0,0,0,0), 0, 0));
		audioControlPane.add(record, new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.LAST_LINE_START, 0, new Insets(0,0,0,0), 0, 0));
		audioControlPane.add(refresh, new GridBagConstraints(2, 0, 1, 1, 1.0, 1.0, GridBagConstraints.LAST_LINE_END, 0, new Insets(0,0,0,0), 0, 0));
		audioControlPane.setBorder(BorderFactory.createLineBorder(new Color(0xBEBEBE)));
		
		audioPane.add(progressBar, new GridBagConstraints(	0 /* gridx */, 
															0 /* gridy */, 
															1 /* gridweight */, 
															1 /* gridheight */, 
															1.0 /* weightx */,
															1.0 /* weighty */,
															GridBagConstraints.FIRST_LINE_START /* Anchor */,
															GridBagConstraints.BOTH/* fill */,
															new Insets(0,0,0,0),
															0 /* ipadx */,
															0 /* ipady*/ ));
		audioPane.add(audioControlPane, new GridBagConstraints(	1 /* gridx */, 
																0 /* gridy */, 
																1 /* gridweight */, 
																1 /* gridheight */, 
																0.0 /* weightx */,
																1.0 /* weighty */,
																GridBagConstraints.FIRST_LINE_END /* Anchor */,
																GridBagConstraints.NONE /* fill */,
																new Insets(0,5,0,0),
																0 /* ipadx */,
																0 /* ipady*/ ));
		
		this.add(audioPane, new GridBagConstraints(	0 /* gridx */, 
													0 /* gridy */, 
													1 /* gridweight */, 
													1 /* gridheight */, 
													1.0 /* weightx */,
													1.0 /* weighty */,
													GridBagConstraints.FIRST_LINE_END /* Anchor */,
													GridBagConstraints.HORIZONTAL /* fill */,
													new Insets(0,0,0,0),
													0 /* ipadx */,
													0 /* ipady*/ ));
	}
	private void reset() {
		progressBar.stop();
		playStop.setEnabled(false);
		record.setEnabled(true);
		recording = false;
		currentAudioData = AudioData.EMPTY_AUDIO;
		playStop.setText(PLAY_BTN_STR);
	}
	
	private void playAudio() {
		audioPlayer.reset();
		playStop.setText(STOP_BTN_STR);						
		try {
			audioPlayer.play(currentAudioData);
			progressBar.start();
		} catch (IOException | LineUnavailableException e1) {
			status.changeStatus(AUDIO_FAILED_TO_PLAY);
		}
	}
	private void _setActions() {
		record.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				record.setEnabled(false);
				progressBar.changeMode(AudioProgressBar.RECORD_MODE, progressBar.getRecordTime());
				prerecordCountdownTimer.scheduleAtFixedRate(new TimerTask() {
					private int countDown = 2;
					@Override
					public void run() {
						if(countDown != 0) {
							characterDisplay.changeCountDown(countDown--);
							characterDisplay.repaint();
						} else {
							characterDisplay.resetCountDown();
							characterDisplay.repaint();
							playStop.setEnabled(true);
							playStop.setText(STOP_BTN_STR);
							progressBar.start();
							status.changeStatus(RECORD_START_STATUS);
							recording = true;
							audioCapture.setRecordFileName(characterDisplay.getCurrentCharacter());
							try {
								audioCapture.start();
							} catch (LineUnavailableException | IOException e) {
								status.changeStatus(RECORD_FAIL_STATUS);
								reset();
							}
							this.cancel();
						}
					}
				}, 0, 1000);
				
				
			}
		});
		playStop.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if(recording) {
					status.changeStatus(RECORD_PREMATURE_STOP_STATUS);
					recording = false;
					playStop.setEnabled(false);
					record.setEnabled(true);
					reset();
					playStop.setText(PLAY_BTN_STR);
				}else {
					if(playStop.getText().equals(STOP_BTN_STR)) {
						playStop.setText(PLAY_BTN_STR);
						if(currentAudioData != null) {
							audioPlayer.stop();
						}
						progressBar.stop();
						record.setEnabled(true);
						refresh.setEnabled(true);
					}else {
						record.setEnabled(false);
						refresh.setEnabled(false);
						playAudio();
					}
				}
			}
		});
		progressBar.addProgressListener(new AudioRecordProgressEvent() {
			@Override
			public void progressFinished() { 
				status.changeStatus(RECORD_FINISHED_STATUS);
				playStop.setText(PLAY_BTN_STR);
				record.setEnabled(true);
				refresh.setEnabled(true);
				recording = false;
				if(progressBar.getMode() == AudioProgressBar.RECORD_MODE) {
					_configureRecordedAudioData();
					if(options.isAutoReplayChecked()) {
						playAudio();
					}
				} else if(progressBar.getMode() == AudioProgressBar.PLAY_MODE) {
					progressBar.stop();
				}
			}
		});
		refresh.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				_configureRecordedAudioData();
			}
		});
	}
	private void _configureRecordedAudioData() {
		try {
			currentAudioData = new AudioData(audioCapture.getRecordFile().getAbsolutePath());
			currentAudioData.getAudioData();
			AudioManipulation.SingleModification(currentAudioData, options.getQuietNoiseBufferValue()); // Used to hear the sample of modified audio.
			//currentAudioData.saveAudio(new File("/Volumes/Audio/Korean Text-to-Speech Audio/test.wav"));
			progressBar.changeMode(AudioProgressBar.PLAY_MODE, (int)Math.ceil(currentAudioData.getDuration()));
		} catch (UnsupportedAudioFileException | InvalidAudioChannelsException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
