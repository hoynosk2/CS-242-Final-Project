package tools.mysql;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Stack;

import linguistics.KoreanSymbol;

/**
 * @description A object that gives direct access to the Korean symbol database and its respective tables.
 * @author dan
 *
 */
public class DatabaseAccess {
	public static final int MASTER_USER = 0; // Master user can read and write to the database.
	public static final int WEB_USER = 1;	// Web user can only read from the database.
	
	private static final String MASTER_USERNAME = "java_master";
	private static final String MASTER_PASSWORD = "javapass";
	// The main database url must specify the character encoding for Korean characters to be encoded correctly.
	private static final String DATABASE_URL = "jdbc:mysql://localhost:3306/korean_symbols?characterSetResults=UTF-8&characterEncoding=UTF-8&useUnicode=yes&useSSL=false";
	
	/* Details information on the Character Audio Version table*/
	private static final String CHARACTER_AUDIO_VERSION_TABLE = "character_audio_version";
	// Represents the columns in the Character Audio Version table.
	private static final String[] AUDIO_VERSION_TITLES = 
	{
			"version1"
	};
	private static final int AUDIO_VERSION_ONE_INDEX = 0;
	
	/* Details information on the Character Symbol table (main table) */
	private static final String CHARACTER_SYMBOLS_TABLE = "character_symbols";
	// Represents the columns in the Character Symbols table
	private static final String[] CHARACTER_SYMBOLS_TABLE_TITLES = 
	{
			"symbol",
			"first_letter",
			"last_letter",
			"stroke_order",
			"government_frequency"
	};
	private static final int SYMBOL_INDEX = 0;
	private static final int FIRST_LETTER_INDEX = 1;
	private static final int LAST_LETTER_INDEX = 2;
	private static final int STROKE_ORDER_INDEX = 3;
	private static final int GOV_FREQ_INDEX = 4;
		
	private Connection mysqlConn;	// The DB connection.
	private Properties login;		// Login properties cannot be changed after the building of the object.
	private final int userType;		// Defines the type of user who has access to this object.
	
	/**
	 * @description Builds an access object to the Korean symbols database for reading and writing purposes.
	 * @param userType The type of user that holds access to this instance.
	 */
	public DatabaseAccess(final int userType) {
		this.userType = userType;
		_configureUser(userType);
	}
	
	/**
	 * @description Configures the user login information; this cannot be undone. A new object must be constructed if this is desired.
	 * @param userType The type of user to configure access too.
	 */
	private void _configureUser(int userType) {
		login = new Properties();
		switch(userType) {
			case MASTER_USER:
				login.setProperty("user", MASTER_USERNAME);
				login.setProperty("password", MASTER_PASSWORD);
				break;
			case WEB_USER:
				// TODO Implement the web user side in week 4.
				break;
			default:
				// TODO Implement the web user side in week 4.
				break;
		}
	}
	
	/**
	 * @description Creates an insert statement for inserting into a particular table.
	 * @param table The table to insert the data into.
	 * @param titles The column titles of the table.
	 * @param values The values to insert (must match respective column title ordering).
	 * @return An insert statement that inserts "values" into "table."
	 */
	protected static String GetInsertStatement(String table, String[] titles, String[] values) {
		// IGNORE is need to avoid duplicate exceptions from occurring.
		return 	"INSERT IGNORE INTO " + table + 
					" (" + String.join(",", titles) + ") " + 
				"VALUES (" + String.join(",", values) + ")";
	}
	
	/**
	 * @description Values must be surrounded by quotes if strings when inputting into the database.
	 * @param str The string to surround quotes with.
	 * @return The string str, but surrounded with quotes.
	 */
	private static String _AddQuotes(String str) {
		return "\"" + str + "\"";
	}
	
	/**
	 * @description Add a new symbol to the symbol database, if present, ignores and does NOT overwrite.
	 * @param symbol The Korean symbol (i.e. character block) to add.
	 * @param strokeOrder The stroke ordering for the Korean symbol.
	 * @param govFreq If available, a government frequency details its frequency in language.
	 * @return True if added successfully, false if duplicate already exists.
	 * @throws SQLException
	 */
	public boolean addSymbolEntry(KoreanSymbol kSymbol) throws SQLException {
		String[] strokeOrder = kSymbol.getStrokeOrder();
		String[] values = 
		{
				_AddQuotes(kSymbol.getSymbol()),						// The main korean symbol.
				_AddQuotes(strokeOrder[0]),							// The first letter that begins the stroke ordering of the symbol.
				_AddQuotes(strokeOrder[strokeOrder.length - 1]),		// The last letter that ends the stroke ordering of the symbol.
				_AddQuotes(String.join(",", strokeOrder)),			// Complete stroke ordering of the symbol.
				_parseGovernmentFrequency(kSymbol.getGovernmentFrequency())	// The government provided frequency detailing how frequent the symbol is in daily usage.
		};
		
		String query = GetInsertStatement(CHARACTER_SYMBOLS_TABLE, CHARACTER_SYMBOLS_TABLE_TITLES, values); // Gets the correct insert statement
		PreparedStatement pstmt = mysqlConn.prepareStatement(query); // Prepared statements are used to avoid any type of mis-hacking that could happen (e.g. string is interrupted as command). 
		int result = pstmt.executeUpdate();							// Updates the datebase. Results details how many rows were touched and modified.
		return (result == 1); 										// True if adding the symbol to the table, else if zero, means duplicated exists.
	}
	
	private String _parseGovernmentFrequency(Integer govFreq) {
		if(govFreq == null)
			return "null";
		else {
			return govFreq.toString();
		}
	}
	
	/**
	 * @description Removes a given symbol from the symbol table.
	 * @param symbol The symbol to remove.
	 * @return True if symbol was removed successfully, otherwise false if symbol was not present.
	 * @throws SQLException
	 */
	public boolean removeSymbolEntry(KoreanSymbol kSymbol) throws SQLException {
		String query = "DELETE FROM " + CHARACTER_SYMBOLS_TABLE + 
						" WHERE symbol=" + _AddQuotes(kSymbol.getSymbol());
		PreparedStatement pstmt = mysqlConn.prepareStatement(query); 	// Prepared statements are used to avoid any type of mis-hacking that could happen (e.g. string is interrupted as command). 
		int result = pstmt.executeUpdate(); 							// Updates the datebase. Results details how many rows were touched and modified.
		return (result == 1);										// True if adding the symbol to the table, else if zero, means duplicated exists.
	}
	
	/**
	 * @description Gets the Korean symbol from the database and its parts if available. 
	 * @param symbol The symbol to pull from the database.
	 * @return A Korean symbol if found, otherwise null.
	 * @throws SQLException
	 */
	public KoreanSymbol getSymbol(String symbol) throws SQLException {
		String query = "SELECT * FROM " + CHARACTER_SYMBOLS_TABLE +
						" WHERE symbol=" + _AddQuotes(symbol);
		PreparedStatement pstmt = mysqlConn.prepareStatement(query); // Prepared statements are used to avoid any type of mis-hacking that could happen (e.g. string is interrupted as command). 
		ResultSet result = pstmt.executeQuery();						// Any results returned back from the query (e.g. a table) can be pulled from a ResultSet.
		KoreanSymbol kSymbol = null;
		if(result.next()) { // If a next exists then configure the Korean symbol, else just return null.
			String[] strokeOrder = result.getString(CHARACTER_SYMBOLS_TABLE_TITLES[STROKE_ORDER_INDEX]).split(",");
			int govFreq = result.getInt(CHARACTER_SYMBOLS_TABLE_TITLES[GOV_FREQ_INDEX]);
			String audioStr = getAudioAddress(symbol);
			kSymbol = new KoreanSymbol(symbol, strokeOrder, govFreq, audioStr);
		}
		return kSymbol;
	}
	
	/**
	 * @description Gets the Korean symbol from the database that matches the stroke ordering provided
	 * @param strokes The stroke order to match.
	 * @return A Korean symbol if found, otherwise null.
	 * @throws SQLException
	 */
	public KoreanSymbol getSymbol(String[] strokes) throws SQLException {
		String strokesStr = _AddQuotes(String.join(",", strokes));
		String query = "SELECT * FROM " + CHARACTER_SYMBOLS_TABLE +
						" WHERE " + CHARACTER_SYMBOLS_TABLE_TITLES[STROKE_ORDER_INDEX] + " LIKE " + strokesStr;
		
		PreparedStatement pstmt = mysqlConn.prepareStatement(query); 	// Prepared statements are used to avoid any type of mis-hacking that could happen (e.g. string is interrupted as command). 
		ResultSet result = pstmt.executeQuery();						// Any results returned back from the query (e.g. a table) can be pulled from a ResultSet.
		KoreanSymbol kSymbol = null;
		if(result.next()) { // If a next exists then configure the Korean symbol, else just return null.
			String symbol = result.getString(CHARACTER_SYMBOLS_TABLE_TITLES[SYMBOL_INDEX]);
			String[] strokeOrder = result.getString(CHARACTER_SYMBOLS_TABLE_TITLES[STROKE_ORDER_INDEX]).split(",");
			int govFreq = result.getInt(CHARACTER_SYMBOLS_TABLE_TITLES[GOV_FREQ_INDEX]);
			String audioStr = getAudioAddress(symbol);
			kSymbol = new KoreanSymbol(symbol, strokeOrder, govFreq, audioStr);
		} 
		return kSymbol;
		
	}
	
	/**
	 * @description Get the audio address for a particular symbol. Details the wave file address on the main server computer.
	 * @param symbol The Korean symbol to lookup
	 * @return The audio file system path to the wave file for the given symbol.
	 * @throws SQLException
	 */
	public String getAudioAddress(String symbol) throws SQLException {
		String query = "SELECT * FROM " + CHARACTER_AUDIO_VERSION_TABLE +
				" WHERE symbol=" + _AddQuotes(symbol);
		PreparedStatement pstmt = mysqlConn.prepareStatement(query); 	// Prepared statements are used to avoid any type of mis-hacking that could happen (e.g. string is interrupted as command). 
		ResultSet result = pstmt.executeQuery();						// Any results returned back from the query (e.g. a table) can be pulled from a ResultSet.
		String address = null;
		if(result.next()) { // If a result exists return it, else just return null.
			address = result.getString(AUDIO_VERSION_TITLES[AUDIO_VERSION_ONE_INDEX]);
			String query2 = "SELECT * FROM character_audio WHERE id=" + address;
			PreparedStatement pstmt2 = mysqlConn.prepareStatement(query2); 	// Prepared statements are used to avoid any type of mis-hacking that could happen (e.g. string is interrupted as command). 
			ResultSet result2 = pstmt2.executeQuery();						// Any results returned back from the query (e.g. a table) can be pulled from a ResultSet.
			if(result2.next()) {
				address = result2.getString("path");
			}
		}
		return address;
	}
	
	/**
	 * @description Gets two Korean symbols that have no audio attached to it. It gets the most frequently appearing symbols first, over less appearing ones.
	 * @return 1. ) Two Korean symbols if found two symbols with no attached audio, or 2.) One Korean symbol that may or may not have attached audio.
	 * @throws SQLException
	 * @note k1.frequency() > k2.frequency()
	 */
	public KoreanSymbol[] getNextFrequentSymbolsWithoutAudio() throws SQLException {
		String query = "SELECT * FROM " + CHARACTER_SYMBOLS_TABLE;
		String audioVersioQuery = "SELECT * FROM " + CHARACTER_AUDIO_VERSION_TABLE + 
									" WHERE symbol=";
		KoreanSymbol k1 = null, k2 = null, firstSymbol = null;
		
		PreparedStatement pstmt = mysqlConn.prepareStatement(query); 	// Prepared statements are used to avoid any type of mis-hacking that could happen (e.g. string is interrupted as command). 
		ResultSet result = pstmt.executeQuery();						// Any results returned back from the query (e.g. a table) can be pulled from a ResultSet.
		
		// Let's grab the first Korean symbol and save it for the end just in case we don't find any unrecorded symbols.
		if(result.next()) {
			/*
			 * Next symbol information
			 */
			String symbol = result.getString(CHARACTER_SYMBOLS_TABLE_TITLES[SYMBOL_INDEX ]);
			String[] strokeOrder = result.getString(CHARACTER_SYMBOLS_TABLE_TITLES[STROKE_ORDER_INDEX]).split(",");
			int govFreq = result.getInt(CHARACTER_SYMBOLS_TABLE_TITLES[GOV_FREQ_INDEX]);
			
			String audioAddress = getAudioAddress(symbol); // The audio address, if available, of the Korean symbol.
			
			firstSymbol = new KoreanSymbol(symbol, strokeOrder, govFreq, audioAddress);
			if(audioAddress == null) // If this firstSymbol just so happens not to have audio attached to it, then allow k1 to be attached to it.
				k1 = firstSymbol;
		}
		while(result.next()) {
			/*
			 * Next symbol information
			 */
			String symbol = result.getString(CHARACTER_SYMBOLS_TABLE_TITLES[SYMBOL_INDEX ]);
			String[] strokeOrder = result.getString(CHARACTER_SYMBOLS_TABLE_TITLES[STROKE_ORDER_INDEX]).split(",");
			int govFreq = result.getInt(CHARACTER_SYMBOLS_TABLE_TITLES[GOV_FREQ_INDEX]);
			
			String audioAddress = getAudioAddress(symbol); // The audio address, if available, of the Korean symbol.
			
			if(k1 == null) {
				// Only grab the first Korean symbol if it has no audio attached to it.
				if(audioAddress != null)
					continue;
				k1 = new KoreanSymbol(symbol, strokeOrder, govFreq, audioAddress);
			} else if(k2 == null) {
				// Grab only the second one if there is no audio address for the following symbol.
				if(audioAddress != null)
					continue;
				k2 = new KoreanSymbol(symbol, strokeOrder, govFreq, audioAddress);
				// We now have two unrecorded symbols, let's order them with K1 > K2.
				if(k1.getGovernmentFrequency() < k2.getGovernmentFrequency()) {
					KoreanSymbol temp = k1;
					k1 = k2;
					k2 = temp;
				}
			} else if(govFreq > k1.getGovernmentFrequency()) {
				// If the next symbol is larger in frequency to k1, then shift everything down. K2 drops off now.
				KoreanSymbol temp = k1;
				k1 = new KoreanSymbol(symbol, strokeOrder, govFreq, audioAddress);
				k2 = temp;
			} else if(govFreq > k2.getGovernmentFrequency()) {
				k2 = new KoreanSymbol(symbol, strokeOrder, govFreq, audioAddress);
			}
		}
		
		// If no symbols were found, then just return the very first symbol.
		if(k1 == null)
			k1 = firstSymbol;
		
		return new KoreanSymbol[] {k1, k2};
	}
	
	/**
	 * @description Opens up the SQL connection.
	 * @throws SQLException
	 */
	public void open() throws SQLException {
		mysqlConn = DriverManager.getConnection(DATABASE_URL, login);
	}
	
	/**
	 * @description Closes up the SQL connection.
	 * @throws SQLException
	 */
	public void close() throws SQLException {
		if(mysqlConn != null)
			mysqlConn.close();
	}
}
