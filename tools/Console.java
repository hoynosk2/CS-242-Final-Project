package tools;

import javax.swing.JOptionPane;
import javax.swing.JFrame;
/**
 * For error and general message output
 * 
 * @author Dan Hoynoski
 */
public class Console{
    public static void simpleOutput(JFrame frame, String output, boolean error){
        String title = error ? "Critical Error" : "Information";
        simpleOutput(frame, title, output, error);
    }
    public static void simpleOutput(JFrame frame, String title, String output, boolean error){
        int messageType = error ? JOptionPane.ERROR_MESSAGE : JOptionPane.QUESTION_MESSAGE;
        JOptionPane.showMessageDialog(frame, output, title, messageType);
    }
    /*
     *  Return: Yes(true) or No(false)
     */
    public static boolean comfirmationDialogBox(JFrame frame, String title, String output, byte error){
        Object[] options = {"No", "Yes"};
        int choice = JOptionPane.showOptionDialog(frame, output, title, 
                                                  JOptionPane.YES_NO_OPTION, 
                                                  (error == 1) ? JOptionPane.ERROR_MESSAGE : JOptionPane.QUESTION_MESSAGE, 
                                                  null, options, options[0]);
        return choice == 1 ? true : false;
    }
}

