package tools.basicinterface;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.SQLException;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.WindowConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import audio.AudioData;
import audio.AudioPlayer;
import audio.InvalidAudioChannelsException;
import identification.Version;
import linguistics.SpecialSymbols;
import tools.Build;
import tools.Console;
import tools.mysql.DatabaseAccess;

public class ApplicationWindow {
	private static final String LISTEN_BTN_STR = "Say";
	private static final String RANDOM_BTN_STR = "I'm Feeling Lucky";
	private static final String INPUT_WATERMARK = "e.g. 대학교에서 컴퓨터공학을 공부하고 있습니다.";
	
	/* Status Strings */
	private static final String DATABASE_CONNECTION_FAILED = "Database failed to connect.";
	
	private JFrame frame; 							// Main frame to display everything in
	private JButton listen, random; 					// Main control buttons for input.
	private DatabaseAccess dba;						// Gives access to the database.
	private PlaceHolderTextField input;				// Text input field.
	
	public ApplicationWindow(final String title) {
		this.frame = new JFrame(title + " " + Version.VERSION_STR);
		this.frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		this.frame.setResizable(false);
		this.setLook(); 																	// Sets the look to be default to the operating system.
		dba = new DatabaseAccess(DatabaseAccess.MASTER_USER); 							// Use Master User as read and write access is needed.
		this.frame.getContentPane().add(this._getMainContentPane(), BorderLayout.CENTER); // Places main content pain in the center of the frame for best viewing. 
		_setActions();
		_initialize();
		this.frame.pack(); // Must be called to reorganize and compact the frame together.
		random.requestFocusInWindow(); 
	}
	
	/**
	 * @description Opens the database connection and configures the character display for the first two characters.
	 */
	private void _initialize() {
		try {
			dba.open();
		} catch (SQLException e) {
			Console.simpleOutput(frame, DATABASE_CONNECTION_FAILED, true);
		}
	}
	
	/**
	 * @description Sets all the actions that correspond to buttons, etc.
	 */
	private void _setActions() {
		random.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String randomSentence = SpecialSymbols.GenerateRandomKoreanSentence();
				input.setText(randomSentence);
			}
		});
		listen.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String inputText = input.getText();
				try {
					AudioData audio = Build.Compile(dba, inputText);
					AudioPlayer player = new AudioPlayer();
					player.play(audio);
				} catch (SQLException | IOException | UnsupportedAudioFileException | InvalidAudioChannelsException | LineUnavailableException e1) {
					e1.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * @description The main content pane that contains all the graphics elements to be displayed on screen. Everything is built in here.
	 * @return The main content pane, which has all the comments inside of.
	 */
	private JPanel _getMainContentPane() {
		JPanel main = new JPanel();
		main.setLayout(new GridBagLayout());
		main.setOpaque(true);				// If allowed to be transparent then background cannot be set.
		main.setBackground(Color.WHITE);
		
		TitledBorder title;
		title = BorderFactory.createTitledBorder(
				BorderFactory.createEtchedBorder(EtchedBorder.LOWERED), "댄 하이노스키의 말 / Dan Hoynoski's Voice");
		title.setTitleJustification(TitledBorder.CENTER);
		main.setBorder(title);
		
		Image scaledIcon = (new ImageIcon(getClass().getResource("images/flag.png"))).getImage().getScaledInstance(75, 75, Image.SCALE_SMOOTH);
		ImageIcon imageIcon = new ImageIcon(scaledIcon);
		JLabel iconLabel = new JLabel(imageIcon, JLabel.CENTER);
		
		main.add(iconLabel, new GridBagConstraints(	0 /* gridx */, 
													0 /* gridy */, 
													2 /* gridweight */, 
													1 /* gridheight */, 
													1.0 /* weightx */,
													1.0 /* weighty */,
													GridBagConstraints.CENTER /* Anchor */,
													GridBagConstraints.BOTH /* fill */,
													new Insets(10,5,5,5),
													0 /* ipadx */,
													0 /* ipady*/ ));
		
		Image scaledTypeIcon = (new ImageIcon(getClass().getResource("images/unnamed.png"))).getImage().getScaledInstance(30, 30, Image.SCALE_SMOOTH);
		ImageIcon typeIcon = new ImageIcon(scaledTypeIcon);
		JLabel typeLabel = new JLabel("");
		typeLabel.setIcon(typeIcon);
		typeLabel.setHorizontalAlignment(SwingConstants.LEFT);
		input = new PlaceHolderTextField(25, INPUT_WATERMARK);
		main.add(input, new GridBagConstraints(	1 /* gridx */, 
												1 /* gridy */, 
												1 /* gridweight */, 
												1 /* gridheight */, 
												1.0 /* weightx */,
												1.0 /* weighty */,
												GridBagConstraints.CENTER /* Anchor */,
												GridBagConstraints.HORIZONTAL /* fill */,
												new Insets(5,0,0,5),
												0 /* ipadx */,
												0 /* ipady*/ ));
		main.add(typeLabel, new GridBagConstraints(	0 /* gridx */, 
													1 /* gridy */, 
													1 /* gridweight */, 
													1 /* gridheight */, 
													1.0 /* weightx */,
													1.0 /* weighty */,
													GridBagConstraints.CENTER /* Anchor */,
													GridBagConstraints.BOTH /* fill */,
													new Insets(5,5,5,0),
													0 /* ipadx */,
													0 /* ipady*/ ));
		
		
		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout());
		buttonPanel.setOpaque(false);
		listen = new JButton(LISTEN_BTN_STR);
		random = new JButton(RANDOM_BTN_STR);
		buttonPanel.add(listen);
		buttonPanel.add(random);
		
		main.add(buttonPanel, new GridBagConstraints(0 /* gridx */, 
													2 /* gridy */, 
													2 /* gridweight */, 
													1 /* gridheight */, 
													1.0 /* weightx */,
													1.0 /* weighty */,
													GridBagConstraints.CENTER /* Anchor */,
													GridBagConstraints.HORIZONTAL /* fill */,
													new Insets(0,0,0,0),
													0 /* ipadx */,
													0 /* ipady*/ ));
		
		
		return main;
	}
	
	/**
     *  @description Toggles the main frame visibility status from on to off.
     */
    public void toggle(){
        if(this.frame.isVisible())
            this.frame.setVisible(false);
        else
            this.frame.setVisible(true);
    }
    
    /**
     * @description Sets the application frame's look and feel to be that of the currently running operating system.
     */
	private void setLook(){
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            ex.printStackTrace();
        }
    }
}
