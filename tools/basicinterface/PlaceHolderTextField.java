package tools.basicinterface;

import javax.swing.JTextField;
import java.awt.Graphics;
import javax.swing.FocusManager;
import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.Font;
import java.awt.RenderingHints;
import javax.swing.text.Document;
public class PlaceHolderTextField extends JTextField{
    /**
	 * Default Serial Version ID
	 */
	private static final long serialVersionUID = 1L;
	private final String placeHolderString;
    public PlaceHolderTextField(final String waterText){
        super();
        this.placeHolderString = waterText;
    }
    public PlaceHolderTextField(Document doc, String text, int columns, final String waterText){
        super(doc, text, columns);
        this.placeHolderString = waterText;
    }
    public PlaceHolderTextField(int columns, final String waterText){
        super(columns);
        this.placeHolderString = waterText;
    }
    public PlaceHolderTextField(String text, final String waterText){
        super(text);
        this.placeHolderString = waterText;
    }
    public PlaceHolderTextField(String text, int columns, final String waterText){
        super(text, columns);
        this.placeHolderString = waterText;
    }
    @Override
    protected void paintComponent(final Graphics g){
        super.paintComponent(g);
        if(getText().isEmpty() && ! (FocusManager.getCurrentKeyboardFocusManager().getFocusOwner() == this)){
            Graphics2D g2 = (Graphics2D)g.create();
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2.setColor(getDisabledTextColor());
            g2.setFont(getFont().deriveFont(Font.ITALIC));
            g2.drawString(placeHolderString, getInsets().left, g.getFontMetrics().getMaxAscent() + getInsets().top);
            g2.dispose();
        }
    }
}