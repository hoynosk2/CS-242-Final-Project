package tools;

import java.io.IOException;
import java.sql.SQLException;

import javax.sound.sampled.UnsupportedAudioFileException;

import audio.AudioData;
import audio.AudioManipulation;
import audio.InvalidAudioChannelsException;
import linguistics.KoreanSymbol;
import linguistics.Transformations;
import tools.mysql.DatabaseAccess;

public class Build {
	
	/**
	 * @description Builds a complete transformed audio file from text.
	 * @param access Database access for creating the audio file.
	 * @param input The input text to create an audio file from.
	 * @return A completely compiled audio file.
	 * @throws SQLException
	 * @throws IOException
	 * @throws UnsupportedAudioFileException
	 * @throws InvalidAudioChannelsException
	 */
	public static AudioData Compile(DatabaseAccess access, String input) throws SQLException, IOException, UnsupportedAudioFileException, InvalidAudioChannelsException {
		KoreanSymbol[] text = new KoreanSymbol[input.length()];
		input = input.replaceAll("\\s+",""); // Remove any spaces.
		
		// Get all the symbols in the text.
		for(int i = 0; i < input.length(); i++) {
			KoreanSymbol next = access.getSymbol(Character.toString(input.toCharArray()[i]));
			text[i] = next;
		}
		text = Transformations.Transform(access, text); // Push on grammar transformations to the input text.
		
		// Get audio for each symbol.
		AudioData[] audio = new AudioData[text.length];
		for(int i = 0; i < audio.length; i++) {
			audio[i] = new AudioData(text[i].getAudioAddress());
			AudioManipulation.SingleModification(audio[i], 1000);
		}
		
		AudioData complete = AudioManipulation.CombineAudio(audio[0], audio[1]); // Compile the first two audio files.
		for(int i = 2; i < audio.length; i++) {
			complete = AudioManipulation.CombineAudio(complete, audio[i]); // Combine all the remaining.
		}
		
		return complete;
	}
}
